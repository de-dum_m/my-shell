##
## Makefile for psu_2013_42sh in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
## 
## Made by  armita_a
## Login   <armita_a@epitech.net>
## 
## Started on  Wed Mar 12 09:09:47 2014 armita_a
## Last update Sun Jun  1 14:48:13 2014 de-dum_m
## Last update Wed May 28 11:56:08 2014 Raphaël Bakhouche
##

NAME	= 42sh

SOURCE	=  src/

GNL	= $(SOURCE)gnl/

START	= $(SOURCE)start/

BUILT	= $(SOURCE)builtins/

TOOLS	= $(SOURCE)tools/

PARSER	= $(SOURCE)parser/

PROCESS	= $(SOURCE)process/

READ	= $(SOURCE)readline/

SRC	= $(SOURCE)main.c				\
	$(GNL)get_next_line.c				\
	$(GNL)my_memset.c				\
	$(START)init_all.c				\
	$(START)get_alias.c				\
	$(START)rc_center.c				\
	$(START)wd_tools.c				\
	$(START)prompt_tools.c				\
	$(START)prompt_script.c				\
	$(START)get_prompt.c				\
	$(BUILT)builtins.c				\
	$(BUILT)exit.c					\
	$(BUILT)echo.c					\
	$(BUILT)echo_tools.c				\
	$(BUILT)disown.c				\
	$(BUILT)cd.c					\
	$(BUILT)cd_tools.c				\
	$(BUILT)export.c				\
	$(BUILT)fg.c					\
	$(BUILT)bg.c					\
	$(BUILT)jobs.c					\
	$(BUILT)env.c					\
	$(BUILT)env_option.c 				\
	$(BUILT)env_tools.c				\
	$(BUILT)egal_env.c				\
	$(BUILT)unset.c	 				\
	$(BUILT)source.c				\
	$(TOOLS)tab_copy.c				\
	$(TOOLS)my_strlen.c				\
	$(TOOLS)my_getnbr.c				\
	$(TOOLS)my_tablen.c				\
	$(TOOLS)find_str_in_tab.c			\
	$(TOOLS)my_str_to_wordtab.c			\
	$(TOOLS)my_str_to_wordtab_dir.c			\
	$(TOOLS)is_in_str.c				\
	$(TOOLS)free.c					\
	$(TOOLS)my_strcmp.c				\
	$(TOOLS)my_strncmp.c				\
	$(TOOLS)my_strdup.c				\
	$(TOOLS)my_strstr.c				\
	$(TOOLS)my_strcat.c				\
	$(TOOLS)my_swap.c				\
	$(TOOLS)my_strcopy.c				\
	$(TOOLS)my_append.c				\
	$(TOOLS)access_test.c				\
	$(TOOLS)append_file_to_fd.c			\
	$(TOOLS)my_printf/my_count_putchar.c		\
	$(TOOLS)my_printf/my_count_put_nbr_base.c	\
	$(TOOLS)my_printf/my_count_put_nbr.c		\
	$(TOOLS)my_printf/my_printf.c			\
	$(TOOLS)my_printf/my_printf_tools.c		\
	$(TOOLS)my_printf/my_put_unsigned_nbr.c		\
	$(PARSER)parser.c				\
	$(PARSER)get_args.c				\
	$(PARSER)clean_str.c				\
	$(PARSER)clean_redirections.c			\
	$(PARSER)check_str.c				\
	$(PARSER)list_proc.c				\
	$(PARSER)tools_redirections.c			\
	$(PARSER)get_redirections.c			\
	$(PARSER)list_input.c				\
	$(PARSER)list_output.c				\
	$(PARSER)about_var.c				\
	$(PARSER)remake_argv.c				\
	$(PARSER)interprete_args.c			\
	$(PARSER)complete_string.c			\
	$(PARSER)job_son_underground.c			\
	$(PROCESS)do_replacements.c			\
	$(PROCESS)do_globbing.c				\
	$(PROCESS)replace_quotes.c			\
	$(PROCESS)path_processing.c			\
	$(PROCESS)get_real_command.c			\
	$(PROCESS)execute.c				\
	$(PROCESS)background.c				\
	$(PROCESS)redirections.c			\
	$(PROCESS)free_proc.c				\
	$(PROCESS)set_all.c				\
	$(PROCESS)add_and_remove.c			\
	$(PROCESS)report.c				\
	$(PROCESS)replace_var_in_str.c			\
	$(READ)about_path.c				\
	$(READ)buf_zero.c				\
	$(READ)clean_screen.c				\
	$(READ)completition.c				\
	$(READ)cut_completition.c			\
	$(READ)edit_curs.c				\
	$(READ)filter_completition.c			\
	$(READ)historic.c				\
	$(READ)move_curs_others.c			\
	$(READ)list_completition.c			\
	$(READ)list_historic.c				\
	$(READ)list_read.c				\
	$(READ)list_to_str.c				\
	$(READ)main.c					\
	$(READ)move_curs_advanced.c			\
	$(READ)move_curs.c				\
	$(READ)my_puts.c				\
	$(READ)path_completition.c			\
	$(READ)push_read.c				\
	$(READ)reverse_str.c				\
	$(READ)search_in_env.c				\
	$(READ)tools_completition.c			\
	$(READ)change_line.c				\
	$(READ)about_termcaps.c				\
	$(READ)manage_signal.c				\
	$(READ)disp_historic.c				\
	$(READ)disp_possible_comp.c			\
	$(READ)go_other_word.c				\
	$(READ)get_historic.c				\
	$(READ)replace_alias.c				\
	$(READ)var_completition.c

OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -Iincludes

$(NAME): $(OBJ)
	@$(CC) $(OBJ) -o $(NAME) -lncurses
	@echo -e "[1;032m$(NAME): [0;032mCompiled successfully[0m"
	@cat .greetings

all:	$(NAME)

clean:
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all

coffee:
	@cat .coffee

.PHONY:	all clean fclean re coffee

/*
** parser.h for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 18:02:37 2014 chapui_s
** Last update Fri May 30 09:01:42 2014 armita_a
*/

#ifndef PARSER_H_
# define PARSER_H_

# include <sys/types.h>

typedef struct		s_input
{
  char			*file;
  char			*heredoc;
  int			is_pipe;
  struct s_input	*next;
}			t_input;

typedef struct		s_output
{
  char			*file;
  int			append_or_not;
  int			is_pipe;
  struct s_output	*next;
}			t_output;

typedef struct		s_shell
{
  char			**env;
  char			**env_exec;
  char			**secret_env;
  char			*prompt;
  char			**prompt_plugins;
  int			have_tty;
  int			need_exit;
  int			is_env_exec;
}			t_shell;

typedef struct		s_proc
{
  char			*cmd;
  char			**cmd_argv;
  char			*path_cmd;
  pid_t			pid;
  pid_t			pgid;
  int			outfd;
  int			infd;
  int			mode;
  int			status;
  int			disowned;
  int			is_builtin;
  t_output		*output;
  t_input		*input;
  struct s_proc		*next;
  struct s_proc		*prev;
}			t_proc;

# define IF_OK		(1)
# define IF_ERR		(2)
# define BACKGROUND	(3)

/*
** about_var.c
*/
int	do_the_job_son(int pipefd[2], t_shell *shell, t_proc *proc);

/*
** about_var.c
*/
char	**remake_argv(t_proc *proc, int cur, char *new);

/*
** about_var.c
*/
void	count_var(char *str, int *i);
void	count_quote(char *str, int *i);
void	pass_var(char *str, int *i);

/*
** check_str.c
*/
int	check_str(char *to_check);

/*
** interprete_args.c
*/
void	rm_quotes(char *str, int is_rm);
int	interprete_args(t_proc *proc, t_shell *shell, char ***files_completition);

/*
** clean_redirections.c
*/
void	clean_redirections(char *str);
void	clean_this_redirection(char *str);

/*
** get_redirections.c
*/
int	get_redirections(t_proc *proc, char **env, char ***files_completition);

/*
** clean_str.c
*/
char	*clean_str(char *str);
int	is_bad_character(char *str);

/*
** get_args.c
*/
int	get_args(t_proc *proc, t_shell *shell, char ***files_completition);

/*
** parser.c
*/
char	*get_this_cmd(char *str, int *i, char *mode);
int	get_mode(char *str, char *mode);
int	go_to_end_cmd(char *str, int end);
int	parser(char *str, t_proc **proc, t_shell *shell, char ***files_comp);

/*
** list_proc.c
*/
int	push_proc(t_proc **proc, char *cmd, char *mode, t_proc **tmp_proc);
int	push_output(t_output **output, char *file, int append, int is_pipe);
int	push_input(t_input **input, char *file, char *heredoc, int is_pipe);

/*
** tools_redirections.c
*/
char	*realloc_me_that(char *dest, char *src);
char	*rm_good_string(char *str);
int	is_same_strings(char *str, char *good_string);

/*
** complete_string.c
*/
char	*complete_string(char *str, char **env, char ***files_completition);

/*
** main.c
*/
t_proc	*go_next(t_proc *proc, int nb);
int	my_exit_in_main(t_proc *proc, int nb);

#endif /* !PARSER_H_ */

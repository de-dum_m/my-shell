/*
** readline.h for includes in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue May  6 23:38:26 2014 chapui_s
** Last update Sun Jun  1 00:28:51 2014 armita_a
*/

#ifndef READLINE_H_
# define READLINE_H_

# define TERM_BUF_SIZE	(4096)
# define SIZE_BUF	(10)
# define HISTORIC_SIZE	(15)

# include <term.h>

typedef struct		s_read
{
  char			c;
  unsigned int		is_current;
  struct s_read		*prev;
  struct s_read		*next;
}			t_read;

typedef struct		s_reader
{
  struct s_historic	*historic;
  struct termios	term_attr;
  struct termios	term_attr_save;
  t_read		*list_read;
  t_read		*clipboard;
  char			**path;
  char			**env;
  char			*prompt;
  int			size_prompt;
  int			size_read;
  int			current;
  int			is_rm;
  int			is_completition;
  int			fd;
  char			*dir_cur;
}			t_reader;

typedef struct		s_historic
{
  t_read		*list_read;
  int			is_cur;
  struct s_historic	*prev;
  struct s_historic	*next;
}			t_historic;

typedef struct		s_completition
{
  char			*file;
  struct s_completition	*prev;
  struct s_completition	*next;
}			t_completition;

typedef struct		s_functions
{  char			str[7];
  int			(*function)(t_reader *reader);
}			t_functions;

/*
** get_historic.c
*/
char	*replace_alias(char *str, int cur, char *found);

/*
** get_historic.c
*/
char	*replace_it(char *str, int cur, char *last_cmd);
char	*search_last_cmd(char *str, t_historic *historic);
int	is_cmd(char *str, int cur);

/*
** disp_possible_comp.c
*/
int	disp_possible_comp(t_reader *reader, t_completition *list);

/*
** manage_signal.c
*/
int	disp_historic(t_historic *historic);

/*
** manage_signal.c
*/
int	manage_signal(int is_signal, t_reader *reader);
void	get_sigint(int signal);

/*
** about_termcaps.c
*/
int	init_termcaps(char **env, struct termios *term,  struct termios *save);

/*
** prompt.c
*/
char	*get_prompt(char **env, char *prompt, int *size);

/*
** about_path.c
*/
char	**get_path(char *path);
char	**list_to_tab(t_completition *list, int nb_files);
int	is_double(char *file, t_completition *list);
int	get_files(char **tab_c, t_completition **list, int *nb);

/*
** buf_zero.c
*/
void	buf_zero(char *buf, int size);

/*
** clean_screen.c
*/
int	clean_screen(t_reader *reader);

/*
** completition.c
*/
int	completition(t_reader *reader);

/*
** cut_completition.c
*/
char	*cut_after_dir(char *s);
char	*cut_before_dir(char *s);
char	*cut_dir_cur(char *dir);
char	**cut_executable(char **word_ptr);
char	*cut_word(char *word);

/*
** edit_curs.c
*/
int	past_clipboard(t_reader *reader);
int	rm_after(t_reader *reader);
int	rm_before(t_reader *reader);

/*
** filter_completition.c
*/
int		is_good_with_list(t_completition *list, int to_add);
void		rm_bad_name(t_completition **list, char *word);
t_completition	**rm_not_executable(char *pa, char *path, t_completition **list);
char		*check_if_slash(char *path);

/*
** historic.c
*/
int	move_down_hist(t_reader *reader);
int	move_up_hist(t_reader *reader);
int	push_historic(t_historic **historic, t_read *list_read);

/*
** list_completition.c
*/
int	get_len_max(t_completition *list);
int	get_size_completition(t_completition *list);
void	free_completition(t_completition *list);

/*
** list_historic.c
*/
char	*list_to_str(t_read *list_read,t_historic **historic);

/*
** list_read.c
*/
int	get_size_list(t_read *list);
t_read	*rm_in_list(t_reader *reader, int nb);
t_read	*create_read(char c);
int	disp_list_read(t_read *list, int fd);

/*
** main.c
*/
char	*read_line(int fd, char ***f_completition, char **env, char *prompt);
int	get_key(t_reader *reader, char *buf);
int	init_shell(char **env, struct termios *term, struct termios *term_save);
void	restore_shell(char **files_completition);

/*
** move_curs_advanced.c
*/
int	go_end(t_reader *reader);
int	go_next_word(t_reader *reader);
int	go_prev_word(t_reader *reader);
int	go_start(t_reader *reader);

/*
** move_curs.c
*/
int	get_char(t_reader *reader, char *buf);
int	go_left(t_reader *reader);
int	go_right(t_reader *reader);
int	rm_cur(t_reader *reader);
int	rm_left(t_reader *reader);

/*
** my_puts.c
*/
int	my_putstr(char *s, int fd);
int	my_putchar(char c);
int	my_putnbr(int nb);
int	my_tputs(char *s, int fd);

/*
** path_completition.c
*/
char	*get_complete_path(char *word, char *path);
char	*get_true_path(char *word, char *dir_cur, char **word_ptr);
int	mod_directory(char *path, t_completition *list);

/*
** push_read.c
*/
char	*list_to_word(t_read *list_read);
int	is_same_list(t_read *l1, t_read *l2);
int	push_at_place(t_read *tmp, char c, int place);
int	push_read(t_read **list, char c, int place);

/*
** reverse_str.c
*/
void	reverse_str(char *str);

/*
** search_in_env.c
*/
char	*search_in_env(char **env, char *var);
int	push_completition(t_completition **list, char *file);
int	search_var_env(char **env, t_completition **list, char *word);
int	complete_history(t_reader *reader, char *word, char *buf);

/*
** tools_completition.c
*/
char	*get_word_current(t_reader *reader);
int	disp_possible_comp(t_reader *reader, t_completition *list);
int	is_char_completition(char c);
int	is_not_first_word(t_reader *reader);

/*
** move_curs_others.c
*/
int	rm_or_quit(t_reader *reader);
int	is_multiline(t_reader *reader);
int	rm_left_without_change_line(t_reader *reader);

/*
** change_line.c
*/
int	line_up(void);
int	line_down(int size);

#endif /* !READLINE_H_ */

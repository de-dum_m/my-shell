/*
** jobs.h for includes in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 17 13:16:31 2014 de-dum_m
** Last update Sat May 31 17:28:55 2014 de-dum_m
*/

#ifndef JOBS_H_
# define JOBS_H_

# include "shell.h"
# include "tools.h"

# define STOPPED	(10)
# define RUNNING	(11)

# define OUT_IS_PIPE(p)	((p) && (p->output) && (p->output->is_pipe == SUCCESS))

typedef struct		s_bgjobs
{
  t_proc		*frontjob;
  t_proc		**active_jobs;
  int			active_jobs_count;
  int			max_job_count;
}			t_bgjobs;

t_bgjobs	g_bgjobs;

#endif /* !JOBS_H_ */

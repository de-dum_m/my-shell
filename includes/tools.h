/*
** tools.h for includes in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Apr 29 14:30:16 2014 armita_a
** Last update Sun Jun  1 23:10:27 2014 de-dum_m
*/

#ifndef TOOLS_H_
# define TOOLS_H_

# define SUCCESS	1
# define FAILURE	0

# ifdef GLOB_TILDE
#  define MYGLOBFLAG	(GLOB_TILDE)
# else
#  define MYGLOBFLAG	(0)
# endif /* !GLOB_TILDE */

# define ABS(x) ((x) < 0) ? -(x) : (x)

# define RC_FILELOC	".42shrc"

#include "shell.h"

typedef struct	s_ps
{
  char		*str;
  int		len;
}		t_ps;

typedef struct	s_prcode
{
  char		code;
  char		*var;
  char		*(*funct)(char *, t_shell *);
}		t_prcode;

/*
** get_alias.c
*/
char	*alias(char *str, int is_init);
int	check_alias(char *str);

/*
** access_test.c
*/
int	access_test_exe(char *file);

/*
** append_file_to_fd.c
*/
int	append_file_to_fd(char *file, int fd);

/*
** find_str_in_tab_c.c
*/
char	*find_str_in_tab(char *str, char **env);

/*
** my_tab_copy.h
*/
char	**my_tab_copy(char **my_tab);

/*
** free.c
*/
void	free_tab(char **tab_c);

/*
** is_in_str.c
*/
int	is_in_str(char *str, char c);

/*
** my_append.c
*/
char	*my_append(char *s1, char *s2);
int	append_one_char(t_ps *pr, char c, int *j);

/*
** my_getnbr.c
*/
int	my_getnbr(char *str);

/*
** my_printf/my_count_putchar.c
*/
int	count_prints();
int	my_count_putchar(char c, int fd);
int	my_count_putstr(char *str, int fd);

/*
** my_printf/my_count_put_nbr_base.c
*/
int	my_count_putnbr_base(unsigned int nbr, char *base, int fd);
int	chop_chop_count(unsigned int nbr, int b, char *base, int fd);

/*
** my_printf/my_count_put_nbr.c
*/
int	my_count_put_nbr(int nb, int fd);

/*
** my_printf/my_printf.c
*/
int	my_print_error(const char *str, ...);
int	my_printf(const char *str, ...);

/*
** my_printf/my_printf_tools.c
*/
int	my_put_hex(unsigned int ix, char type, int fd);
int	my_put_ptr(unsigned int ptr, int fd);
int	my_putstr_weird(char *str, int fd);

/*
** my_printf/my_put_unsigned_nbr.c
*/
int	my_put_unsigned_nbr(unsigned int nb, int fd);
int	print_modified_count(char nb, int fd);

/*
** my_strcat.c
*/
char	*str_cat(char *s1, char *s2);

/*
** my_strcmp.c
*/
int	my_strcmp(char *s1, char *s2);

/*
** my_strdup.c
*/
char	*my_strdup(const char *s);

/*
** my_strlen.c
*/
int	my_strlen(char *s);

/*
** my_strncmp.c
*/
int	my_strncmp(char *s1, char *s2, int n);

/*
** my_strstr.c
*/
char	*my_strstr(char *str, char *to_find);

/*
** my_str_to_wordtab.c
*/
char	**my_str_to_wordtab_dir(char *str);
char	**my_str_to_wordtab(char *str);

/*
** my_swap.c
*/
void	my_swap_str(char **s1, char **s2);

/*
** my_tab_clen.c
*/
int	my_tablen(char **tab_c);

/*
** my_strcopy.c
*/
char	*my_str_copy(char *str, int size);
int	have_var(char *str);

/*
** get_prompt.c
*/
char	*prompt_script(char *str, t_shell *shell);
t_ps	*put_prompt(char *base, t_shell *shell);

/*
** prompt_tools.c
*/
char	*get_username(char *nothing, t_shell *shell);
char	*get_hostname(char *nothing, t_shell *shell);
char	*prompt_get_wd(char *ignore, t_shell *shell);
char	*prompt_kw_char(char *str, t_shell *shell);
char	*get_homedir();

/*
** rc_center.c
*/
int	parse_rc_file(t_shell *shell);

/*
** wd_tools.c
*/
char	*prompt_mini_wd(char *ignore_this, t_shell *shell);

#endif /* !TOOLS_H_ */

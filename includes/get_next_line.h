/*
** get_next_line.h for includes in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:28:41 2014 armita_a
** Last update Sun Jun  1 00:28:42 2014 armita_a
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define BUFSIZ	42

typedef struct	s_fd
{
  char		*str;
  int		fd;
  struct s_fd	*next;
}		t_fd;

char	*get_next_line(const int fd);
char	*my_memset(char *str, int len);

#endif /* !GET_NEXT_LINE_H_ */

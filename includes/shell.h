/*
** shell.h for includes in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/includes
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri May  2 10:43:32 2014 armita_a
** Last update Fri May 30 16:30:39 2014 de-dum_m
*/

#ifndef SHELL_H_
# define SHELL_H_

# include <signal.h>
# include "parser.h"

# define NOAPPEND	(O_RDWR | O_TRUNC | O_CREAT | O_CLOEXEC)
# define APPEND		(O_RDWR | O_APPEND | O_CREAT | O_CLOEXEC)
# define RIGHTS		(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
# define OP_MODE(x)	((x) == 1) ? (APPEND) : (NOAPPEND)

# ifdef WCOREDUMP
#  define SHCOREDUMP(x)	(WCOREDUMP(x)) ? ("(core dumped)") : (" ")
# else
#  define SHCOREDUMP(x) (x) ? (" ") : (" ")
# endif /* !WCOREDUMP */

# define MYDIR		"."

typedef struct	s_signal_msg
{
  int		signal_num;
  char		*msg;
}		t_signal_msg;

typedef struct	s_opec
{
  char		*str;
  int		(*funct)(t_shell *, t_proc *);
}		t_opec;

/*
** background.c
*/
int	put_to_background(t_proc *proc, int status);
int	put_to_foreground(t_proc *proc, char **env);
void	report_signal(t_proc *proc, int status, int mode, int job_id);
void	update_process_info(int pid, int status);
int	close_all(t_proc *proc);
void	check_jobs(int signum);

/*
** globbing.c
*/
int	put_glob(t_proc *proc, int *i);
int	glob_and_replace(t_proc *proc, t_shell *shell);
void	replace_quotes(t_proc *proc, int *i, t_shell *shell, int quote_mode);

/*
** add_and_remove.c
*/
int	add_job(t_proc *proc);
int	is_in_jobs_list(pid_t pid);
int	remove_job(pid_t pid);

/*
** report.c
*/
void	report_stop(pid_t pid, int status);
void	report_continuation(pid_t pid, int status);
void	report_termination(pid_t pid, int status);
int	pgid_alive(pid_t pgid, pid_t pid);
void	print_sig_msg(t_proc *proc, int status, int job_id);

/*
** get_real_command.c
*/
int	inpath_execute(t_proc *proc, t_shell *shell);
int	direct_execute(t_proc *proc);

/*
** execute.c
*/
int	my_execute(t_proc *proc, t_shell *shell);
void	set_all_signals(int nb);
int	my_loop_proc(int nb, t_proc *proc, t_shell *shell);

/*
** set_all.c
*/
int	set_redirections(t_proc *proc);
char	*replace_var_in_str(char *str, t_shell *shell);

/*
** free_proc.c
*/
void	free_proc(t_proc *proc);
t_proc	*proc_copy(t_proc *proc);
t_proc	*super_proc(t_proc *proc);

/*
** path_processing.c
*/
char	**process_path(char **env);

/*
** redirections.c
*/
int	put_redirections(t_proc *proc);

/*
** init_all.c
*/
char	**create_new_env(char **env);
int	init_all(t_shell *elem, char **env);
void	reset_all(char **env, char **files_completition);
void	relaunch(int nb);

/*
** readline.c
*/
char	*readline(int fd, char ***files_completition, char **env);

#endif /* !SHELL_H_ */

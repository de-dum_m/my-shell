/*
** builtins.h for includes in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Thu May  1 14:27:16 2014 armita_a
** Last update Sun Jun  1 00:27:45 2014 armita_a
*/

#ifndef BUILTINS_H_
# define BUILTINS_H_

# include "shell.h"

# define MAX_BUILT_FORK	8
# define MAX_BUILT	11

typedef struct	s_buil
{
  char		*str;
  int		(*f_tab)(t_shell*, char *, int *, int *);
}		t_buil;

typedef struct	s_echo
{
  char		exec_val;
  char		next_val;
}		t_echo;

/*
** builtins.c
*/
int	builtins(t_shell *elem, t_proc *proc);
int	check_builtins(t_proc *proc);
int	built_no_fork(t_proc *proc, t_shell *shell);

/*
** cd.c
*/
int	my_cd(t_shell *shell, t_proc *proc);

/*
** disown.c
*/
int	my_disown(t_shell *shell, t_proc *proc);

/*
** cd_tools.c
*/
char	*my_strcat_spe(char *var, char *str);
int	char_only(char *str, char c);
void	fill_new_cd(char *new, char *str);

/*
** fg.c
*/
int	my_fg(t_shell *elem, t_proc *proc);

/*
** jobs.c
*/
int	my_jobs(t_shell *elem, t_proc *proc);

/*
** bg.c
*/
int	my_bg(t_shell *elem, t_proc *proc);

/*
** echo.c
*/
int	my_echo(t_shell *elem, t_proc *proc);

/*
** echo_tools.c:
*/
char	*get_var(t_shell *shell, t_proc *proc, int index, int *count);
char	*clean_var(char *str);

/*
** env.c
*/
int	my_env(t_shell *elem, t_proc *proc);
int	my_print_env(char **env, int val);

/*
** env.c
*/
int	my_egal_env(t_shell *elem, t_proc *proc);
int	test_var(char *str);

/*
** env_option.c
*/
int	help_env(t_shell *elem, char *str, int *val, int *index);
int	null_env(t_shell *elem, char *str, int *val, int *cursor);
int	reset_env(t_shell *elem, char *str, int *val, int *index);
int	unset_env(t_shell *elem, char *str, int *val, int *cursor);
int	my_add_at_env(t_shell *elem, char *str);

/*
** env_tools.c
*/
void	add_str_end_env(t_shell *shell, char *str);
void	remplace_str_in_env(t_shell *shell, char *str, char *var);
void	get_new_cmd(char *new, int i, char **tab_cmd);
int	get_new_size(int i, char **tab_cmd);

/*
** exit.c
*/
int	my_exit(t_shell *shell, t_proc *proc);

/*
** export.c
*/
int	my_export(t_shell *elem, t_proc*proc);

/*
** unset.c
*/
int	my_unset(t_shell *elem, t_proc*proc);

/*
** source.c
*/
int	my_source(t_shell *shell, t_proc *proc);

#endif /* !BUILTINS_H_ */

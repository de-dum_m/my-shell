/*
** cd_tools.c for builtins in /home/bakhou_r/projet/42sh/builtins
**
** Made by Raphaël Bakhouche
** Login   <bakhou_r@epitech.net>
**
** Started on  Mon May 19 16:20:06 2014 Raphaël Bakhouche
** Last update Sun Jun  1 00:36:26 2014 armita_a
*/

#include "tools.h"

char	*my_strcat_spe(char *var, char *str)
{
  int	i;
  int	n;

  i = my_strlen(str) + my_strlen(var) - 1;
  str[i + 1] = '\0';
  n = my_strlen(str) - 1;
  while (n >= 0)
    str[i--] = str[n--];
  n = 0;
  i = 0;
  while (var[n])
    str[i++] = var[n++];
  return (str);
}

int	char_only(char *str, char c)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] != c)
	return (0);
      ++i;
    }
  return (1);
}

void	fill_new_cd(char *new, char *str)
{
  int	i;
  int	n;

  i = 2;
  n = 2;
  new[0] = '.';
  new[1] = '.';
  while (str[i])
    {
      new[n++] = '/';
      new[n++] = '.';
      new[n++] = '.';
      ++i;
    }
  new[n] = '\0';
}

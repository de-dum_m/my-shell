/*
** export.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/builtins
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon May  5 21:10:22 2014 armita_a
** Last update Sun Jun  1 00:40:43 2014 armita_a
*/

#include <stdlib.h>
#include "builtins.h"
#include "tools.h"

static void	find_my_var(t_shell *shell, char *str, int *val, int *index)
{
  int		count;
  char		*temp;
  int		cursor;

  while (shell->secret_env && shell->secret_env[*index] && *val == 0)
    {
      count = 0;
      while (shell->secret_env[*index][count]
	     && shell->secret_env[*index][count] != '=')
	count++;
      if (!(temp = malloc(sizeof(char) * (count + 1))))
	return ;
      cursor = 0;
      while (cursor < count)
	{
	  temp[cursor] = shell->secret_env[*index][cursor];
	  cursor++;
	}
      temp[cursor] = '\0';
      if (my_strcmp(temp, str) == 0)
	*val += 1;
      else
	*index += 1;
      free(temp);
    }
}

static void	export_one_in_env(t_shell *shell, char *str)
{
  int		index;
  int		val;
  t_shell	shell_temp;

  val = 0;
  index = 0;
  find_my_var(shell, str, &val, &index);
  if (val == 1)
    {
      add_str_end_env(shell, shell->secret_env[index]);
      shell_temp.env = shell->secret_env;
      unset_env(&shell_temp, str, &val, &val);
      shell->secret_env = shell_temp.env;
    }
}

static void	export_all_in_env(t_shell *shell)
{
  int		index;

  index = 0;
  while (shell->secret_env && shell->secret_env[index])
    add_str_end_env(shell, shell->secret_env[index++]);
  free_tab(shell->secret_env);
  shell->secret_env = NULL;
}

int	my_export(t_shell *shell, t_proc *proc)
{
  int	index;

  if (!proc || !proc->cmd_argv || !proc->cmd_argv[1])
    {
      export_all_in_env(shell);
      my_print_env(shell->env, SUCCESS);
      return (SUCCESS);
    }
  index = 1;
  while (proc && proc->cmd_argv && proc->cmd_argv[index])
    {
      if (is_in_str(proc->cmd_argv[index], '=') == SUCCESS)
	{
	  if (my_add_at_env(shell, proc->cmd_argv[index]) == FAILURE)
	    return (SUCCESS);
	}
      else
	export_one_in_env(shell, proc->cmd_argv[index]);
      index++;
    }
  return (SUCCESS);
}

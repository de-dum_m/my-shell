/*
** unset.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 17 15:23:26 2014 armita_a
** Last update Sun May 25 12:23:58 2014 armita_a
*/

#include <stdlib.h>
#include "builtins.h"
#include "tools.h"

static int	unset_in_shell(t_shell *shell, char *str)
{
  int		val;

  if (!(find_str_in_tab(str, shell->env)))
    return (FAILURE);
  unset_env(shell, str, &val, &val);
  return (SUCCESS);
}

static int	unset_in_secret_tab(t_shell *shell, char *str)
{
  t_shell	shell_temp;
  int		val;

  if (!(find_str_in_tab(str, shell->secret_env)))
    return (FAILURE);
  shell_temp.env = shell->secret_env;
  unset_env(&shell_temp, str, &val, &val);
  return (SUCCESS);
}

int	my_unset(t_shell *shell, t_proc *proc)
{
  int	index;

  proc->status = 1;
  if (!proc->cmd_argv || !proc->cmd_argv[1])
    {
      my_print_error("unset: not enough arguments\n");
      return (SUCCESS);
    }
  index = 1;
  while (proc && proc->cmd_argv && proc->cmd_argv[index])
    {
      if ((unset_in_secret_tab(shell, proc->cmd_argv[index])) == FAILURE)
	if ((unset_in_shell(shell, proc->cmd_argv[index])) == FAILURE)
	  return (SUCCESS);
      index++;
    }
  proc->status = 0;
  return (SUCCESS);
}

/*
** bg.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/builtins
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri May 16 18:24:05 2014 armita_a
** Last update Sun Jun  1 00:35:19 2014 armita_a
*/

#include "jobs.h"

static int	no_jobs(t_proc *proc)
{
  my_print_error("bg: no jobs to resume\n");
  proc->status = 1;
  return (SUCCESS);
 }

int	my_bg(t_shell *shell, t_proc *proc)
{
  int	job_id;

  (void)shell;
  job_id = -1;
  proc->status = 0;
  if (g_bgjobs.active_jobs_count == 0)
    return (no_jobs(proc));
  if (!(proc->cmd_argv[1]) && g_bgjobs.active_jobs_count > 0
      && g_bgjobs.active_jobs[g_bgjobs.active_jobs_count - 1])
    job_id = g_bgjobs.active_jobs_count - 1;
  else if (proc->cmd_argv[1] && ((job_id = my_getnbr(proc->cmd_argv[1])) < 0
				 || job_id >= g_bgjobs.active_jobs_count))
    my_print_error("42sh: no such job [%s]\n", proc->cmd_argv[1]);
  if (job_id > -1 && job_id < g_bgjobs.active_jobs_count)
    {
      g_bgjobs.active_jobs[job_id]->mode = BACKGROUND;
      if (put_to_background(g_bgjobs.active_jobs[job_id], 0) == FAILURE)
	my_print_error("bg: putting to background failed\n");
    }
  return (SUCCESS);
}

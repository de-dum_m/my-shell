/*
** builtins.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/builtins
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Thu May  1 14:28:38 2014 armita_a
** Last update Sun Jun  1 00:35:52 2014 armita_a
*/

#include <string.h>
#include "tools.h"
#include "builtins.h"

static const t_opec	g_bul[] =
   {
     {"exit", &my_exit},
     {"cd", &my_cd},
     {"fg", &my_fg},
     {"bg", &my_bg},
     {"jobs", &my_jobs},
     {"unset", &my_unset},
     {"export", &my_export},
     {"disown", &my_disown},
     {"source", &my_source},
     {"env", &my_env},
     {"echo", &my_echo},
     {NULL, NULL},
   };

int	builtins(t_shell *shell, t_proc *proc)
{
  if (g_bul[proc->is_builtin].str)
    return (g_bul[proc->is_builtin].funct(shell, proc));
  else
    return (my_egal_env(shell, proc));
}

int	check_builtins(t_proc *proc)
{
  int	index;

  index = 0;
  proc->is_builtin = -1;
  while (g_bul[index].str)
    {
      if (proc->cmd && strcmp(proc->cmd, g_bul[index].str) == 0)
	{
	  proc->is_builtin = index;
	  return (SUCCESS);
	}
      index++;
    }
  if (is_in_str(proc->cmd, '=') == SUCCESS)
    {
      proc->is_builtin = index;
      return (SUCCESS);
    }
  return (FAILURE);
}

int	built_no_fork(t_proc *proc, t_shell *shell)
{
  if (proc->is_builtin < MAX_BUILT_FORK - 1)
    return (g_bul[proc->is_builtin].funct(shell, proc));
  else
    return (builtins(shell, proc));
}

/*
** source.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 31 23:50:11 2014 armita_a
** Last update Sun Jun  1 00:41:17 2014 armita_a
*/

#include <unistd.h>
#include "tools.h"

int	my_source(t_shell *shell, t_proc *proc)
{
  (void)proc;
  alias(NULL, -1);
  parse_rc_file(shell);
  return (SUCCESS);
}

/*
** exit.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/builtins
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Mon May  5 21:08:37 2014 armita_a
** Last update Sun Jun  1 00:40:19 2014 armita_a
*/

#include <unistd.h>
#include "jobs.h"
#include "builtins.h"

static int	wave_goodbye_to_the_kids()
{
  int		i;

  i = 0;
  while (i < g_bgjobs.max_job_count)
    {
      if (g_bgjobs.active_jobs[i]
	  && g_bgjobs.active_jobs[i]->disowned == FAILURE)
	if (kill(g_bgjobs.active_jobs[i]->pid, SIGHUP) == -1)
	  return (FAILURE);
      i++;
    }
  return (SUCCESS);
}

static int	pre_exit_check()
{
  char		c;

  if (g_bgjobs.active_jobs_count > 0)
    {
      my_print_error("You have suspended jobs, exit? [y/N] ");
      if (read(0, &c, 1) == 1)
	{
	  if (c == 'Y' || c == 'y')
	    return (wave_goodbye_to_the_kids());
	  else
	    return (FAILURE);
	}
      my_print_error("\n");
    }
  return (SUCCESS);
}

int	my_exit(t_shell *shell, t_proc *proc)
{
  int	nb;

  (void)shell;
  if (pre_exit_check() == FAILURE)
    return (SUCCESS);
  if (!(proc->cmd_argv[1]))
    shell->need_exit = 0;
  else
    {
      nb = my_getnbr(proc->cmd_argv[1]);
      nb = (unsigned char)nb;
      shell->need_exit = nb;
    }
  return (SUCCESS);
}

/*
** jobs.c for builtins in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 17 11:07:26 2014 de-dum_m
** Last update Sat May 31 23:46:45 2014 armita_a
*/

#include <stdlib.h>
#include "jobs.h"
#include "builtins.h"

static void	simple_job_list()
{
  int		i;

  i = 0;
  while (i < g_bgjobs.max_job_count)
    {
      if (g_bgjobs.active_jobs[i])
	my_printf("[%d] running %s\n", i, g_bgjobs.active_jobs[i]->cmd);
      ++i;
    }
}

static void	precise_info(int job_id)
{
  my_printf("[%d] %d running  %s\n", job_id, g_bgjobs.active_jobs[job_id]->pid,
            g_bgjobs.active_jobs[job_id]->cmd);
}

int	my_jobs(t_shell *shell, t_proc *proc)
{
  int	job_id;

  (void)shell;
  proc->status = 0;
  if (!(proc->cmd_argv[1]))
    simple_job_list();
  else if (proc->cmd_argv[1] && (job_id = my_getnbr(proc->cmd_argv[1])) >= 0
	   && job_id < g_bgjobs.active_jobs_count)
    precise_info(job_id);
  else if (proc->cmd_argv[1])
    {
      proc->status = 1;
      my_print_error("jobs: no such job [%s]\n", proc->cmd_argv[1]);
      return (SUCCESS);
    }
  return (SUCCESS);
}

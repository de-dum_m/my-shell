/*
** disown.c for builtins in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat May 24 20:01:04 2014 de-dum_m
** Last update Sun Jun  1 00:36:48 2014 armita_a
*/

#include "jobs.h"

int	my_disown(t_shell *shell, t_proc *proc)
{
  int	job_id;

  (void)shell;
  proc->status = 0;
  job_id = -1;
  if (g_bgjobs.active_jobs_count == 0)
    {
      my_print_error("disown: no jobs to disown\n");
      proc->status = 1;
      return (SUCCESS);
    }
  if (!(proc->cmd_argv[1]) && g_bgjobs.active_jobs_count > 0
      && g_bgjobs.active_jobs[g_bgjobs.active_jobs_count - 1])
    job_id = g_bgjobs.active_jobs_count - 1;
  else if (proc->cmd_argv[1] && ((job_id = my_getnbr(proc->cmd_argv[1])) < 0
				 || job_id >= g_bgjobs.active_jobs_count))
    my_print_error("disown: no such job [%s]\n", proc->cmd_argv[1]);
  if (job_id > -1 && job_id < g_bgjobs.active_jobs_count)
    g_bgjobs.active_jobs[job_id]->disowned = SUCCESS;
  return (SUCCESS);
}

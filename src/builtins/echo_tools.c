/*
** echo_tools.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 18 11:02:40 2014 armita_a
** Last update Sun Jun  1 00:38:00 2014 armita_a
*/

#include <stdlib.h>

char	*clean_var(char *str)
{
  int	index;
  int	count;
  int	cursor;
  char	*temp;

  index = 0;
  if (!str)
    return (NULL);
  while (str && str[index] && str[index] != '=')
    index++;
  count = index + 1;
  while (str && str[index])
    index++;
  if (!(temp = malloc(sizeof(char) * (index - count + 1))))
    return (NULL);
  cursor = 0;
  while (str[count])
    temp[cursor++] = str[count++];
  temp[cursor] = '\0';
  return (temp);
}

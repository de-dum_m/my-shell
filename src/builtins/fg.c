/*
** fg.c for builtins in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/builtins
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri May 16 18:23:43 2014 armita_a
** Last update Sun Jun  1 00:40:52 2014 armita_a
*/

#include <unistd.h>
#include "builtins.h"
#include "jobs.h"

static int	my_print_error_fg(char *str, char *name, t_proc *proc)
{
  proc->status = 1;
  if ((my_print_error(str, name)) == -1)
    return (FAILURE);
  return (SUCCESS);
}

static int	fg_solo()
{
  if (g_bgjobs.active_jobs_count > 0
      && g_bgjobs.active_jobs[g_bgjobs.active_jobs_count - 1])
    return (g_bgjobs.max_job_count - 1);
  else
    my_print_error("fg: please specify a job_id\n");
  return (-1);
}

static int	my_baby_fg(t_shell *shell, t_proc *proc)
{
  int		job_id;

  job_id = -1;
  proc->status = 0;
  if (g_bgjobs.active_jobs_count == 0)
    return (my_print_error_fg("fg: no jobs to resume\n", NULL, proc));
  if (!(proc->cmd_argv[1]))
    job_id = fg_solo();
  else if (proc->cmd_argv[1] && ((job_id = my_getnbr(proc->cmd_argv[1])) < 0
				 || job_id >= g_bgjobs.max_job_count
				 || !(g_bgjobs.active_jobs[job_id])))
    return (my_print_error_fg("42sh: no such job [%s]\n",
			      proc->cmd_argv[1], proc));
  if (job_id > -1 && job_id < g_bgjobs.max_job_count)
    {
      if (put_to_foreground(g_bgjobs.active_jobs[job_id],
			    shell->env) == FAILURE)
	return (my_print_error_fg("fg: putting to foreground failed\n",
				  NULL, proc));
    }
  return (SUCCESS);
}

int	my_fg(t_shell *shell, t_proc *proc)
{
  if (isatty(0) && isatty(1))
    return (my_baby_fg(shell, proc));
  else
    my_print_error("fg: no job control in this shell\n");
  proc->status = 1;
  return (SUCCESS);
}

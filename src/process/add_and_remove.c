/*
** add_and_remove.c for 42sh in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun May 25 19:48:51 2014 de-dum_m
** Last update Sun May 25 23:02:19 2014 de-dum_m
*/

#include <stdlib.h>
#include "jobs.h"

int	add_job(t_proc *proc)
{
  int	i;

  i = 0;
  while (i < g_bgjobs.max_job_count)
    {
      if (!(g_bgjobs.active_jobs[i]))
	{
	  g_bgjobs.active_jobs_count++;
	  g_bgjobs.active_jobs[i] = proc;
	  return (i);
	}
      i++;
    }
  g_bgjobs.active_jobs_count++;
  g_bgjobs.max_job_count++;
  if (!(g_bgjobs.active_jobs = realloc(g_bgjobs.active_jobs, sizeof(t_proc *) *
				       (g_bgjobs.max_job_count + 1))))
    return (-1);
  g_bgjobs.active_jobs[i] = proc;
  return (i);
}

static int	get_biggest_job()
{
  int		i;
  int		max;

  i = 0;
  max = 0;
  while (g_bgjobs.active_jobs && i < g_bgjobs.max_job_count - 1)
    {
      if (g_bgjobs.active_jobs[i])
	max = i;
      i++;
    }
  return (max);
}

int	is_in_jobs_list(pid_t pid)
{
  int	i;

  i = 0;
  while (i < g_bgjobs.max_job_count)
    {
      if (g_bgjobs.active_jobs[i] && g_bgjobs.active_jobs[i]->pid == pid)
	return (i);
      i++;
    }
  return (-1);
}

int	remove_job(pid_t pid)
{
  int	job_id;

  if ((job_id = is_in_jobs_list(pid)) == -1)
    return (-1);
  g_bgjobs.active_jobs[job_id] = NULL;
  g_bgjobs.active_jobs_count--;
  if (job_id == g_bgjobs.max_job_count - 1)
    {
      g_bgjobs.max_job_count = get_biggest_job();
      if (!(g_bgjobs.active_jobs =
	    realloc(g_bgjobs.active_jobs,
		    sizeof(t_proc *) * (g_bgjobs.max_job_count + 1))))
	return (-1);
    }
  return (job_id);
}

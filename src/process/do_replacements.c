/*
** rid_quote.c for process in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu May 29 12:05:04 2014 de-dum_m
** Last update Sun Jun  1 17:01:27 2014 de-dum_m
*/

#include <stdlib.h>
#include "shell.h"
#include "tools.h"

static int	get_quote_mode(char *str)
{
  int		i;

  i = 0;
  while (str && str[i])
    {
      if ((str[i] == '"' || str[i] == '\'') && (i == 0 || str[i - 1] != '\\'))
	return (str[i]);
      i++;
    }
  return (-1);
}

static int	is_glob_str(char *str)
{
  int		i;
  int		j;
  const char	*glob_char;

  i = 0;
  glob_char = "*~?[]";
  while (str && str[i])
    {
      j = 0;
      while (glob_char[j] && str[i] != glob_char[j])
	j++;
      if (str[i] == glob_char[j])
	return (i);
      i++;
    }
  return (-1);
}

int	glob_and_replace(t_proc *proc, t_shell *shell)
{
  int		i;
  int		quote_mode;

  i = 0;
  while (proc->cmd_argv && proc->cmd_argv[i])
    {
      if ((quote_mode = get_quote_mode(proc->cmd_argv[i])) != -1)
	replace_quotes(proc, &i, shell, quote_mode);
      else if (is_glob_str(proc->cmd_argv[i]) >= 0)
	{
	  if (put_glob(proc, &i) == FAILURE)
	    return (FAILURE);
	}
      else
	{
	  proc->cmd_argv[i] = replace_var_in_str(proc->cmd_argv[i], shell);
	  ++i;
	}
    }
  proc->cmd = replace_var_in_str(proc->cmd, shell);
  return (SUCCESS);
}

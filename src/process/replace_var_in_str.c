/*
** replace_var_in_str.c for process in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun May 25 00:09:36 2014 armita_a
** Last update Sun Jun  1 00:23:28 2014 armita_a
*/

#include <stdlib.h>
#include "builtins.h"
#include "tools.h"
#include "shell.h"

static char	*get_var_name(char *str, int nb)
{
  int		index;
  char		*name;

  index = nb;
  while (str && str[index] && str[index] != ' ' && str[index] != '/'
	 && str[index] != '$' &&  str[index] != '=' && str[index] != ':')
    index++;
  if (!(name = malloc(sizeof(char *) * (index - nb + 1))))
    return (NULL);
  name[index - nb] = '\0';
  index = 0;
  while (str && str[nb] && str[nb] != ' ' && str[nb] != '='
	 && str[nb] != '$' && str[nb] != ':' && str[nb] != '/')
    name[index++] = str[nb++];
  return (name);
}

static char	*get_var_replace(char *name, t_shell *shell)
{
  char		*ptr;

  if (!(ptr = find_str_in_tab(name, shell->env)))
    if (!(ptr = find_str_in_tab(name, shell->secret_env)))
      return (NULL);
  return (clean_var(ptr));
}

static char	*replace_val_in_str(char *str, char *name, char *val, int nb)
{
  char		*temp;
  int		index;
  int		cursor;

  if (!( temp = malloc(sizeof(char *) *(my_strlen(str) + my_strlen(val) + 1
					- my_strlen(name)))))
    return (NULL);
  index = 0;
  cursor = 0;
  while (index < nb)
    temp[index++] = str[cursor++];
  nb = 0;
  while (val && val[nb])
    temp[index++] = val[nb++];
  cursor += my_strlen(name) + 1;
  while (str && str[cursor])
    temp[index++] = str[cursor++];
  temp[index] = '\0';
  free(name);
  free(val);
  free(str);
  return (temp);
}

char	*replace_var_in_str(char *str, t_shell *shell)
{
  int	nb;
  char	*name;
  char	*val;

  while (str && (nb = have_var(str)) != -1)
    {
      name = get_var_name(str, nb + 1);
      val = get_var_replace(name, shell);
      str = replace_val_in_str(str, name, val, nb);
    }
  return (str);
}

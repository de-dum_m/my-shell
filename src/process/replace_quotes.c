/*
** replace_quotes.c for process in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu May 29 14:49:56 2014 de-dum_m
** Last update Sun Jun  1 17:02:31 2014 de-dum_m
*/

#include <stdlib.h>
#include "shell.h"
#include "tools.h"

char	*move_over(char *str, int i)
{
  if (!(str = realloc(str, my_strlen(str))))
    return (NULL);
  while (str && str[i])
    {
      str[i] = str[i + 1];
      ++i;
    }
  return (str);
}

char	*destroy_quotes(char *str, char quote)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      if (str && str[i] && str[i] == quote && (i == 0 || str[i - 1] != '\\'))
	str = move_over(str, i--);
      ++i;
    }
  return (str);
}

void	replace_quotes(t_proc *proc, int *i, t_shell *shell, int quote_mode)
{
  proc->cmd_argv[*i] = destroy_quotes(proc->cmd_argv[*i], quote_mode);
  if (quote_mode == '"')
    proc->cmd_argv[*i] = replace_var_in_str(proc->cmd_argv[*i], shell);
  ++(*i);
}

/*
** path_processing.c for 42sh in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue May 13 11:45:24 2014 de-dum_m
** Last update Sat May 24 17:00:31 2014 armita_a
*/

#include <stdlib.h>
#include "tools.h"

static int	next_single_path_len(char *path)
{
  int		i;

  i = 0;
  while (path[i])
    if (path[i++] == ':')
      return (i);
  return (i);
}

static int	path_len(char *path)
{
  int		i;
  int		len;

  i = 0;
  len = 0;
  while (path && path[i])
    if (path[i++] == ':')
      len++;
  return (len);
}

static char	**path_loop(char **res, char *path_str, int *x, int *j)
{
  int		i;

  i = 0;
  while (path_str[i])
    {
      if (path_str[i] == ':')
	{
	  if (res[*j][*x - 1] != '/')
	    res[*j][(*x)++] = '/';
	  res[*j][*x] = '\0';
	  *x = 0;
	  if (!(res[++(*j)] = malloc(next_single_path_len(path_str + ++i) + 2)))
	    return (NULL);
	}
      res[*j][(*x)++] = path_str[i++];
    }
  return (res);
}

char	**process_path(char **env)
{
  int	i;
  int	j;
  int	x;
  char	**res;
  char	*path_str;

  j = 0;
  x = 0;
  i = 0;
  if (!(path_str = find_str_in_tab("PATH", env))
      || !(res = malloc(sizeof(char *) * (path_len(path_str) + 2)))
      || !(res[j] = malloc(next_single_path_len(path_str) + 2)))
    return (NULL);
  while (path_str[i] && path_str[i++] != '=');
  res = path_loop(res, path_str + i, &x, &j);
  if (res[j][x - 1] != '/')
    res[j][x++] = '/';
  res[j][x] = '\0';
  res[++j] = NULL;
  return (res);
}

/*
** report.c for process in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/process
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 24 11:00:27 2014 armita_a
** Last update Sun Jun  1 17:01:12 2014 de-dum_m
*/

#include <unistd.h>
#include <stdlib.h>
#include "jobs.h"

static const t_signal_msg	g_sig_msg[] =
  {
    {SIGHUP, "hangup"},
    {SIGINT, "interrupted"},
    {SIGQUIT, "quit"},
    {SIGABRT, "abort"},
    {SIGFPE, "floating point exception"},
    {SIGSEGV, "segmentation fault"},
    {SIGPIPE, "broken pipe"},
    {SIGALRM, "alarm"},
    {SIGTERM, "terminated"},
    {SIGUSR1, "user defined signal 1"},
    {SIGUSR2, "user defined signal 2"},
    {SIGCONT, "continued"},
    {SIGSTOP, "stopped"},
    {SIGTSTP, "stopped at terminal"},
    {SIGTTIN, "tty input"},
    {SIGTTOU, "tty output"},
    {FAILURE, NULL}
  };

void	print_sig_msg(t_proc *proc, int status, int job_id)
{
  int	i;

  i = 0;
  if (WIFSIGNALED(status))
    {
      i = 0;
      while (g_sig_msg[i].signal_num != FAILURE
	     && g_sig_msg[i].signal_num != WTERMSIG(status))
	i++;
      if (g_sig_msg[i].signal_num != FAILURE)
	my_printf("[%d]	%d	%s %s %s\n", job_id, (int)proc->pid,
		  g_sig_msg[i].msg, SHCOREDUMP(status),proc->cmd);
    }
  else if (WIFSTOPPED(status))
    {
      i = 0;
      while (g_sig_msg[i].signal_num != FAILURE
	     && g_sig_msg[i].signal_num != WSTOPSIG(status))
	i++;
      if (g_sig_msg[i].signal_num != FAILURE)
	my_printf("[%d]	%d %s	%s\n", job_id,
		  (int)proc->pid, g_sig_msg[i].msg, proc->cmd);
    }
}

static void	report_termination_end(int status, int job_id)
{
  t_proc	*tmp;

  tmp = g_bgjobs.active_jobs[job_id];
  report_signal(g_bgjobs.active_jobs[job_id], status, 1, job_id);
  close_all(g_bgjobs.active_jobs[job_id]);
  remove_job(tmp->pid);
  free_proc(tmp);
  free(tmp);
}

void		report_termination(pid_t pid, int status)
{
  int		pgid;
  int		job_id;
  t_proc	*tmp;

  if ((job_id = is_in_jobs_list(pid)) == -1 && g_bgjobs.frontjob
      && pid == g_bgjobs.frontjob->pid)
    {
      close_all(g_bgjobs.frontjob);
      report_signal(g_bgjobs.frontjob, status, 0, 0);
      tmp = g_bgjobs.frontjob;
      pgid = tmp->pgid;
      g_bgjobs.frontjob = NULL;
      if ((job_id = pgid_alive(pgid, pid)) != -1)
	{
 	  free_proc(tmp);
	  free(tmp);
	  tmp = g_bgjobs.active_jobs[job_id];
	  g_bgjobs.frontjob = proc_copy(g_bgjobs.active_jobs[job_id]);
	  remove_job(g_bgjobs.active_jobs[job_id]->pid);
	  free_proc(tmp);
	  free(tmp);
	}
    }
  else if (job_id != -1)
    report_termination_end(status, job_id);
}

void	report_continuation(pid_t pid, int status)
{
  int	job_id;

  (void)status;
  if ((job_id = is_in_jobs_list(pid)) != -1)
    {
      if (g_bgjobs.active_jobs[job_id]->mode != BACKGROUND)
	tcsetpgrp(0, g_bgjobs.active_jobs[job_id]->pgid);
      g_bgjobs.active_jobs[job_id]->status = RUNNING;
      my_printf("[%d]	%d continued	%s\n", job_id,
		(int)g_bgjobs.active_jobs[job_id]->pid,
		g_bgjobs.active_jobs[job_id]->cmd);
    }
}

void	report_stop(pid_t pid, int status)
{
  pid_t	pgid;
  int	job_id;

  if ((job_id = is_in_jobs_list(pid)) == -1)
    {
      put_to_background(proc_copy(g_bgjobs.frontjob), STOPPED);
      report_signal(g_bgjobs.frontjob, status, 0, 0);
      pgid = g_bgjobs.frontjob->pgid;
      g_bgjobs.frontjob = NULL;
      if ((job_id = pgid_alive(pgid, pid)) != -1)
	{
	  g_bgjobs.frontjob = proc_copy(g_bgjobs.active_jobs[job_id]);
	  remove_job(g_bgjobs.active_jobs[job_id]->pid);
	}
    }
  else
    {
      g_bgjobs.active_jobs[job_id]->status = STOPPED;
      report_signal(g_bgjobs.active_jobs[job_id], status, 0, job_id);
    }
}

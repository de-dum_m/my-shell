/*
** set_all.c for process in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/process
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 24 10:34:36 2014 armita_a
** Last update Sun Jun  1 17:00:00 2014 de-dum_m
*/

#include <unistd.h>
#include "jobs.h"

int	have_var(char *str)
{
  int	index;

  index = 0;
  while (str && str[index])
    {
      if (str[index] == '$' && ((index == 0 || str[index - 1] != '\\')
				&&  str[index + 1]  != '('))
	return (index);
      index++;
    }
  return (-1);
}

int	pgid_alive(pid_t pgid, int pid)
{
  int	i;

  i = 0;
  while (i < g_bgjobs.max_job_count)
    {
      if (g_bgjobs.active_jobs[i] && g_bgjobs.active_jobs[i]->pgid == pgid
	  && g_bgjobs.active_jobs[i]->pid != pid
	  && g_bgjobs.active_jobs[i]->status == RUNNING)
	return (i);
      i++;
    }
  return (-1);
}

int	set_redirections(t_proc *proc)
{
  if (proc->outfd != FAILURE)
    {
      if (dup2(proc->outfd, 1) == -1)
	return (FAILURE);
      close(proc->outfd);
    }
  if (proc->infd != FAILURE)
    {
      if (dup2(proc->infd, 0) == -1)
	return (FAILURE);
      close(proc->infd);
    }
  if (proc->output && proc->output->is_pipe == SUCCESS && proc->next)
    {
      proc->next->pgid = proc->pgid;
      if (close(proc->next->infd) == -1)
	return (FAILURE);
    }
  return (SUCCESS);
}

void	set_all_signals(int nb)
{
  (void)nb;
  signal(SIGINT, SIG_DFL);
  signal(SIGTSTP, SIG_DFL);
  signal(SIGSTOP, SIG_DFL);
  signal(SIGQUIT, SIG_DFL);
  signal(SIGCHLD, SIG_DFL);
  signal(SIGTTIN, SIG_DFL);
  signal(SIGTTOU, SIG_DFL);
}

int	close_all(t_proc *proc)
{
  if (proc->outfd > 0)
    {
      if (close(proc->outfd) == -1)
	return (FAILURE);
    }
  if (proc->infd > 0)
    {
      if (close(proc->infd) == -1)
	return (FAILURE);
    }
  return (SUCCESS);
}

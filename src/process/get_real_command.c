/*
** get_real_command.c for process in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun May 25 20:42:22 2014 de-dum_m
** Last update Sun Jun  1 17:02:19 2014 de-dum_m
*/

#include <stdlib.h>
#include <unistd.h>
#include "shell.h"
#include "tools.h"

int	direct_execute(t_proc *proc)
{
  if (access_test_exe(proc->cmd) == FAILURE)
    return (FAILURE);
  proc->path_cmd = my_strdup(proc->cmd);
  return (SUCCESS);
}

int	inpath_execute(t_proc *proc, t_shell *shell)
{
  int	i;
  char	*tmp;
  char	**tmp_path;

  i = 0;
  proc->path_cmd = NULL;
  if (!(tmp_path = process_path(shell->env)))
    return (FAILURE);
  while (tmp_path[i] && proc->cmd && !(proc->path_cmd))
    {
      if (access((tmp = my_append(tmp_path[i], proc->cmd)), X_OK) == 0)
        proc->path_cmd = tmp;
      else if (access(tmp, F_OK) == 0)
	return (my_print_error("%s: permission denied\n", proc->cmd));
      else
	free(tmp);
      i++;
    }
  free_tab(tmp_path);
  return (SUCCESS);
}

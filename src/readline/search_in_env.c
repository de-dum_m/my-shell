/*
** search_in_env.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Tue May  6 23:48:39 2014 chapui_s
** Last update Fri May 16 17:59:34 2014 armita_a
*/

#include <stdlib.h>
#include "readline.h"
#include "tools.h"

char	*search_in_env(char **env, char *var)
{
  int	i;
  int	j;
  char	*value;
  int	size_var;

  i = 0;
  j = 0;
  value = NULL;
  if (env == NULL || env[0] == NULL)
    return (NULL);
  if ((size_var = my_strlen(var)) == 0)
    return (NULL);
  while (env && env[i] && my_strncmp(var, env[i], size_var) != 0)
    i += 1;
  if (env[i] == NULL)
    return (NULL);
  while (env[i] && env[i][j] && env[i][j] != '=')
    j += 1;
  j += 1;
  value = &(env[i][j]);
  return (value);
}

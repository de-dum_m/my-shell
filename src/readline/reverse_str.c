/*
** reverse_str.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 01:40:11 2014 chapui_s
** Last update Sat May 24 10:27:03 2014 armita_a
*/

#include "readline.h"
#include "tools.h"

void	reverse_str(char *str)
{
  char	tmp;
  int	begin;
  int	end;

  begin = 0;
  end = my_strlen(str) - 1;
  while (begin <= end)
    {
      tmp = str[begin];
      str[begin] = str[end];
      str[end] = tmp;
      begin += 1;
      end -= 1;
    }
}

/*
** var_completition.c for readline in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri May 16 16:36:26 2014 armita_a
** Last update Sun Jun  1 00:23:11 2014 armita_a
*/

#include <stdlib.h>
#include "readline.h"
#include "tools.h"

static int	get_size_var(char *var)
{
  int		size;

  size = 0;
  while (var[size] && var[size] != '=')
    size += 1;
  return (size);
}

static char	*copy_var(char *var)
{
  char		*tmp;
  int		size;
  int		i;

  size = get_size_var(var);
  if ((tmp = (char*)malloc(size + 2)) == NULL)
    return ((void*)(unsigned long)my_putstr("error: could not alloc\n", 2));
  tmp[0] = '$';
  i = 1;
  while (var[i - 1] && var[i - 1] != '=')
    {
      tmp[i] = var[i - 1];
      i += 1;
    }
  tmp[i] = 0;
  return (tmp);
}

int	search_var_env(char **env, t_completition **list, char *word)
{
  char	*tmp;
  int	i;

  i = 0;
  while (env && env[i])
    {
      if ((tmp = copy_var(env[i])) == NULL)
	return (-1);
      if (my_strncmp(tmp, word, my_strlen(word)) == 0)
	if ((push_completition(list, tmp)) == -1)
	  return (-1);
      free(tmp);
      i += 1;
    }
  return (0);
}

int	complete_history(t_reader *reader, char *word, char *buf)
{
  int	i;
  char	tmp[2];

  i = my_strlen(word);
  tmp[1] = 0;
  while (i >= 0 && i < 7)
    {
      tmp[0] = buf[i];
      if ((get_char(reader, tmp)) == -1)
	return (-1);
      i += 1;
    }
  return (0);
}

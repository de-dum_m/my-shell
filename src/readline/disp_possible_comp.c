/*
** disp_possible_comp.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sun May 25 05:00:03 2014 chapui_s
** Last update Sun Jun  1 00:18:33 2014 armita_a
*/

#include <unistd.h>
#include "readline.h"

static int	loop_disp_comp(t_completition *list)
{
  while (list)
    {
      if ((my_putstr("\n", 1)) == -1
	  || (my_putstr(list->file, 1)) == -1)
	return (-1);
      list = list->next;
    }
  return (0);
}

static int	ask_continue(int nb)
{
  char		buf;
  int		check;

  if (((my_putstr("\nDisplay all ", 1)) == -1)
      || ((my_putnbr(nb)) == -1)
      || ((my_putstr(" possibilities ? (y or n)", 1)) == -1))
    return (-1);
  while ((check = read(0, &buf, 1)) > 0)
    {
      if (buf == 'y')
	return (1);
      else if (buf == 'n')
	return (0);
    }
  if (check == -1)
    return (-1);
  return (0);
}

int	disp_possible_comp(t_reader *reader, t_completition *list)
{
  int	check;
  int	size_list;

  check = 1;
  if (list->next == (void*)0)
    return (0);
  if ((size_list = get_size_completition(list)) > 40)
    if ((check = ask_continue(size_list)) == -1)
      return (-1);
  if (check)
    if ((loop_disp_comp(list)) == -1)
      return (-1);
  if ((my_putstr("\n", 1)) == -1)
    return (-1);
  if (reader->prompt)
    if ((my_putstr(reader->prompt, reader->fd)) == -1)
      return (-1);
  if ((disp_list_read(reader->list_read, 1)) == -1)
    return (-1);
  reader->is_completition = 0;
  return (0);
}

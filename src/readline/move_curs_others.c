/*
** move_curs_others.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 21 21:23:35 2014 chapui_s
** Last update Sun Jun  1 00:22:19 2014 armita_a
*/

#include <sys/ioctl.h>
#include <unistd.h>
#include "readline.h"
#include "tools.h"

int	rm_or_quit(t_reader *reader)
{
  if (reader->list_read)
    {
      rm_cur(reader);
      return (0);
    }
  else
    {
      if ((write(1, "\n", 1)) == -1)
	return (-1);
      if ((my_tputs(tgetstr("ei", (void*)0), 1)) == -1)
	return (-1);
      if ((tcsetattr(1, TCSANOW, &(reader->term_attr_save))) == -1)
	return (my_putstr("error: tcsetattr\n", 2));
      return (-1);
    }
}

int			is_multiline(t_reader *reader)
{
  struct winsize	win;
  int			size_prompt;
  int			size_cmd;

  size_prompt = 20;
  size_cmd = get_size_list(reader->list_read);
  if ((ioctl(1, TIOCGWINSZ, &win)) == -1)
    return (-1);
  if (size_cmd + size_prompt > win.ws_col)
    return (1);
  return (0);
}

int	rm_left_without_change_line(t_reader *reader)
{
  if (reader->current > 0)
    {
      if (((my_tputs(tgetstr("le", NULL), reader->fd)) == -1)
	  || (my_tputs(tgetstr("dc", NULL), reader->fd)) == -1)
	return (-1);
      if (reader->current - 1 >= 0)
	reader->current -= 1;
      reader->list_read = rm_in_list(reader, reader->current);
    }
  return (0);
}

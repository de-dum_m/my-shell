/*
** disp_historic.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 24 22:34:25 2014 chapui_s
** Last update Sun Jun  1 00:18:15 2014 armita_a
*/

#include "readline.h"

int	disp_historic(t_historic *historic)
{
  if (!(historic))
    return (0);
  while (historic && historic->next)
    historic =  historic->next;
  if (historic && historic->prev)
    if ((my_putstr("-> ", 1)) == -1)
      return (-1);
  while (historic && historic->prev)
    {
      if ((disp_list_read(historic->list_read, 1)) == -1)
	return (-1);
      if ((my_putstr("\n", 1)) == -1)
	return (-1);
      historic = historic->prev;
      if (historic->prev)
	if ((my_putstr("-> ", 1)) == -1)
	  return (-1);
    }
  return (0);
}

/*
** move_curs_advanced.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May  7 02:35:55 2014 chapui_s
** Last update Sun May 25 05:35:40 2014 chapui_s
*/

#include <unistd.h>
#include "readline.h"

int	go_start(t_reader *reader)
{
  if ((is_multiline(reader)) == 1)
    return (0);
  if (reader->list_read)
    {
      while (reader->current > 0)
	{
	  if ((my_tputs(tgetstr("le", NULL), reader->fd)) == -1)
	    return (-1);
	  reader->current -= 1;
	}
      if (reader->current == 0)
	reader->is_rm = -1;
    }
  return (0);
}

int	go_end(t_reader *reader)
{
  char	str[4];
  int	size;

  if (is_multiline(reader) == 1)
    return (0);
  str[0] = 27;
  str[1] = 91;
  str[2] = 67;
  str[3] = 0;
  size = get_size_list(reader->list_read);
  while (reader->current < size)
    {
      if ((write(1, str, 4)) == -1)
	return (-1);
      reader->is_rm = 0;
      reader->current += 1;
    }
  return (0);
}

/*
** clean_screen.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May  7 01:51:52 2014 chapui_s
** Last update Sun May 25 05:21:43 2014 chapui_s
*/

#include <curses.h>
#include "readline.h"

int	clean_screen(t_reader *reader)
{
  if ((my_tputs(tgetstr("cl", NULL), reader->fd)) == -1)
    return (-1);
  if (reader->prompt)
    if ((my_putstr(reader->prompt, reader->fd)) == -1)
      return (-1);
  if ((disp_list_read(reader->list_read, reader->fd)) == -1)
    return (-1);
  reader->current = get_size_list(reader->list_read);
  return (0);
}

/*
** list_completition.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Mon May 12 01:00:31 2014 chapui_s
** Last update Fri May 16 17:47:36 2014 armita_a
*/

#include <stdlib.h>
#include "readline.h"
#include "tools.h"

static t_completition	*create_completition(char *file)
{
  t_completition	*new;
  int			i;

  if ((new = (t_completition*)malloc(sizeof(*new))))
    {
      i = 0;
      new->file = NULL;
      if ((new->file = (char*)malloc(my_strlen(file) + 2)) == NULL)
	return ((void*)(unsigned long)my_putstr("error: malloc\n", 1));
      while (file && file[i])
	{
	  new->file[i] = file[i];
	  i += 1;
	}
      new->file[i] = 0;
      new->file[i + 1] = 0;
      new->prev = NULL;
      new->next = NULL;
    }
  return (new);
}

int			push_completition(t_completition **list, char *file)
{
  t_completition	*tmp;

  tmp = *list;
  if (tmp)
    {
      while (tmp->next)
	tmp = tmp->next;
      if ((tmp->next = create_completition(file)) == NULL)
	return (-1);
      tmp->next->prev = tmp;
    }
  else
    if ((*list = create_completition(file)) == NULL)
      return (-1);
  return (0);
}

int	get_size_completition(t_completition *list)
{
  int	size;

  size = 0;
  while (list)
    {
      size += 1;
      list = list->next;
    }
  return (size);
}

void			free_completition(t_completition *list)
{
  t_completition	*tmp;

  while (list)
    {
      tmp = list;
      list = list->next;
      free(tmp->file);
      free(tmp);
    }
}

int	get_len_max(t_completition *list)
{
  int	max;
  int	tmp;

  max = 0;
  while (list)
    {
      if ((tmp = my_strlen(list->file)) > max)
	max = tmp;
      list = list->next;
    }
  return (max);
}

/*
** move_curs.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May  7 01:16:25 2014 chapui_s
** Last update Sun May 25 05:20:36 2014 chapui_s
*/

#include <sys/ioctl.h>
#include <unistd.h>
#include "readline.h"

int	get_char(t_reader *reader, char *buf)
{
  int	i;

  i = 0;
  while (buf[i] >= 32 && i <= SIZE_BUF)
    {
      reader->current += 1;
      if (reader->is_rm == -1)
	reader->current = 0;
      if ((push_read(&(reader->list_read), buf[i], reader->current)) == -1)
	return (-1);
      if (reader->is_rm == -1)
	reader->current = 1;
      reader->is_rm += 1;
      if ((write(reader->fd, &(buf[i]), 1)) == -1)
	return (-1);
      i += 1;
    }
  return (0);
}

int	go_left(t_reader *reader)
{
  int	ret;

  ret = 0;
  if (reader->list_read)
    {
      if ((ret = line_up()) == -1)
	return (-1);
      else if (ret == 0)
	{
	  if (reader->current > 0)
	    if ((my_tputs(tgetstr("le", NULL), reader->fd)) == -1)
	      return (-1);
	}
      if (reader->current - 1 >= 0)
	reader->current -= 1;
      if (reader->current == 0)
	reader->is_rm = -1;
    }
  return (0);
}

int	rm_left(t_reader *reader)
{
  int	ret;

  if (reader->current > 0)
    {
      if ((ret = line_up()) == -1)
	return (-1);
      else if (ret == 0)
	if ((my_tputs(tgetstr("le", NULL), reader->fd)) == -1)
	  return (-1);
      if ((my_tputs(tgetstr("dc", NULL), reader->fd)) == -1)
	return (-1);
      if (reader->current - 1 >= 0)
	reader->current -= 1;
      reader->list_read = rm_in_list(reader, reader->current);
    }
  return (0);
}

int	rm_cur(t_reader *reader)
{
  if ((my_tputs(tgetstr("dc", NULL), reader->fd)) == -1)
    return (-1);
  reader->list_read = rm_in_list(reader, reader->current);
  return (0);
}

int			go_right(t_reader *reader)
{
  struct winsize	win;

  if (reader->current < get_size_list(reader->list_read))
    {
      if ((ioctl(1, TIOCGWINSZ, &win)) == -1)
	return (-1);
      if ((line_down(win.ws_col)) == -1)
	return (-1);
      reader->is_rm = 0;
      reader->current += 1;
    }
  return (0);
}

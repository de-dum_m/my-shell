/*
** replace_alias.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sun May 25 18:12:42 2014 chapui_s
** Last update Sun Jun  1 16:55:21 2014 de-dum_m
*/

#include <stdlib.h>
#include "readline.h"
#include "tools.h"

char	*replace_alias(char *str, int cur, char *found)
{
  char	*new;
  int	i;
  int	j;

  if ((new = (char*)malloc(my_strlen(str) + my_strlen(found) + 2)) == NULL)
    return ((void*)(unsigned long)my_putstr("error: could not alloc\n", 1));
  i = 0;
  while (str[i] && i < cur)
    {
      new[i] = str[i];
      i += 1;
    }
  j = 0;
  while (found[j])
    new[i++] = found[j++];
  j = cur;
  while (str[j] && str[j] != ' ' && str[j] != ';' && str[j] != '|'
	 && str[j] != '&')
    j += 1;
  while (str[j])
    new[i++] = str[j++];
  new[i] = 0;
  free(str);
  return (new);
}

/*
** buf_zero.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 02:38:49 2014 chapui_s
** Last update Fri May 23 01:41:33 2014 chapui_s
*/

void	buf_zero(char *buf, int size)
{
  int	i;

  i = 0;
  while (i < size)
    {
      buf[i] = 0;
      i += 1;
    }
}

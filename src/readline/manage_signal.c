/*
** manage_signal.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 24 22:11:13 2014 chapui_s
** Last update Sun Jun  1 00:21:52 2014 armita_a
*/

#include "readline.h"

int			manage_signal(int is_signal, t_reader *reader)
{
  static t_reader	*ptr;

  if (is_signal == 0)
    ptr = reader;
  else if (ptr)
    {
      ptr->list_read = (void*)0;
      ptr->current = 0;
      ptr->is_rm = 0;
      ptr->is_completition = 0;
      if (ptr->prompt)
	{
	  if ((my_putstr("\n", 1)) == -1)
	    return (-1);
	  if ((my_putstr(ptr->prompt, 1)) == -1)
	    return (-1);
	}
    }
  else
    if ((my_putstr("\n$> ", 1)) == -1)
      return (-1);
  return (0);
}

void	get_sigint(int signal)
{
  (void)signal;
  manage_signal(1, (void*)0);
}

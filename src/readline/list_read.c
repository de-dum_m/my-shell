/*
** list_read.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May  7 00:58:34 2014 chapui_s
** Last update Sun May 25 05:17:07 2014 chapui_s
*/

#include <stdlib.h>
#include <unistd.h>
#include "readline.h"

t_read		*create_read(char c)
{
  t_read	*new;

  new = NULL;
  if ((new = (t_read*)malloc(sizeof(*new))))
    {
      new->c = c;
      new->is_current = 0;
      new->prev = NULL;
      new->next = NULL;
    }
  else
    if ((my_putstr("error: could not alloc\n", 2)) == -1)
      return (NULL);
  return (new);
}

t_read		*rm_in_list(t_reader *reader, int nb)
{
  t_read	*tmp;
  int		i;

  i = 0;
  tmp = reader->list_read;
  if (tmp == NULL)
    return (NULL);
  if (nb == 0)
    {
      reader->list_read = tmp->next;
      if (reader->list_read)
	reader->list_read->prev = NULL;
      return (reader->list_read);
    }
  while (i++ < nb && tmp)
    tmp = tmp->next;
  if (tmp == NULL)
    return (reader->list_read);
  if (tmp && tmp->prev)
    tmp->prev->next = tmp->next;
  if (tmp->next)
    tmp->next->prev = tmp->prev;
  free(tmp);
  return (reader->list_read);
}

int		get_size_list(t_read *list)
{
  t_read	*tmp;
  int		size;

  size = 0;
  tmp = list;
  while (tmp)
    {
      tmp = tmp->next;
      size += 1;
    }
  return (size);
}

int	disp_list_read(t_read *list, int fd)
{
  while (list)
    {
      if ((write(fd, &(list->c), 1)) == -1)
	return (-1);
      list = list->next;
    }
  return (0);
}

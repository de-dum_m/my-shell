/*
** edit_curs.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Thu May  8 19:23:43 2014 chapui_s
** Last update Sun Jun  1 00:19:02 2014 armita_a
*/

#include <stdlib.h>
#include "readline.h"

static void	clean_clipboard(t_reader *reader)
{
  t_read	*tmp;
  t_read	*tmp_to_rm;

  tmp = reader->clipboard;
  while (tmp)
    {
      tmp_to_rm = tmp;
      tmp = tmp->next;
      free(tmp_to_rm);
    }
  reader->clipboard = NULL;
}

static char	get_char_cur(t_reader *reader)
{
  t_read	*tmp;
  int		i;

  i = 0;
  tmp = reader->list_read;
  while (tmp && i < reader->current)
    {
      tmp = tmp->next;
      i += 1;
    }
  if (tmp)
    return (tmp->c);
  return (reader->list_read->c);
}

int		rm_after(t_reader *reader)
{
  t_read	*tmp;
  int		i;

  i = 0;
  if (is_multiline(reader) == 1)
    return (0);
  tmp = reader->list_read;
  clean_clipboard(reader);
  while (tmp && i < reader->current)
    {
      tmp = tmp->next;
      i += 1;
    }
  i = 1;
  while (tmp)
    {
      if ((push_read(&(reader->clipboard), get_char_cur(reader), i)) == -1)
	return (-1);
      rm_cur(reader);
      i += 1;
      tmp = tmp->next;
    }
  return (0);
}

int		rm_before(t_reader *reader)
{
  t_read	*tmp;
  int		i;

  i = 0;
  if (is_multiline(reader) == 1)
    return (0);
  tmp = reader->list_read;
  clean_clipboard(reader);
  while (tmp && tmp->next && i < reader->current)
    {
      if ((push_read(&(reader->clipboard), tmp->c, i + 1)) == -1)
	return (-1);
      tmp = tmp->next;
      i += 1;
    }
  while (tmp)
    {
      if ((rm_left_without_change_line(reader)) == -1)
	return (-1);
      tmp = tmp->prev;
    }
  return (0);
}

int		past_clipboard(t_reader *reader)
{
  t_read	*tmp;
  char		buf[2];

  buf[1] = 0;
  tmp = reader->clipboard;
  while (tmp)
    {
      buf[0] = tmp->c;
      if ((get_char(reader, buf)) == -1)
	return (-1);
      tmp = tmp->next;
    }
  return (0);
}

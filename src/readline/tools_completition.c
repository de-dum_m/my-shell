/*
** tools_completition.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 01:47:15 2014 chapui_s
** Last update Sun May 25 05:07:25 2014 chapui_s
*/

#include <stdlib.h>
#include <unistd.h>
#include "readline.h"

int	is_char_completition(char c)
{
  if (c != ' ' && c != '"' && c != '|' && c != '`'
      && c != '\'' && c != ';')
    return (1);
  return (0);
}

char		*get_word_current(t_reader *reader)
{
  char		*word;
  t_read	*tmp;
  t_read	*tmp_word;
  int		i;

  i = 1;
  tmp_word = NULL;
  tmp = reader->list_read;
  while (i < reader->current && tmp && tmp->next)
    {
      tmp = tmp->next;
      i += 1;
    }
  i = 1;
  while (tmp && is_char_completition(tmp->c))
    {
      if (push_read(&tmp_word, tmp->c, i) == -1)
	return (NULL);
      tmp = tmp->prev;
      i += 1;
    }
  if ((word = list_to_word(tmp_word)))
    reverse_str(word);
  return (word);
}

int		is_not_first_word(t_reader *reader)
{
  t_read	*tmp;
  int		i;

  i = 1;
  tmp = reader->list_read;
  while (tmp && tmp->next && i < reader->current)
    {
      i += 1;
      tmp = tmp->next;
    }
  while (tmp->prev && is_char_completition(tmp->c))
    tmp = tmp->prev;
  while (tmp->prev && tmp->c == ' ')
    tmp = tmp->prev;
  if (is_char_completition(tmp->c) == 0 || tmp->prev == NULL)
    return (0);
  return (1);
}

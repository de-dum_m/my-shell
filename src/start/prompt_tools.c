/*
** prompt_tools.c for 42sh in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun May 25 18:47:38 2014 de-dum_m
** Last update Sun Jun  1 14:48:38 2014 de-dum_m
*/

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#include "tools.h"
#include "builtins.h"

char	*prompt_kw_char(char *str, t_shell *shell)
{
  (void)shell;
  return (my_str_copy(str, 0));
}

char	*prompt_get_wd(char *ignore, t_shell *shell)
{
  (void)ignore;
  (void)shell;
  return (getcwd(NULL, 0));
}

char		*get_homedir()
{
  struct passwd	*pwd;
  char		*homedir;

  if (!(pwd = getpwuid(getuid())))
    return (NULL);
  else
    homedir = my_str_copy(pwd->pw_dir, 0);
  endpwent();
  return (homedir);
}

char		*get_username(char *nothing, t_shell *shell)
{
  struct passwd	*pwd;
  char		*username;

  (void)nothing;
  (void)shell;
  if (!(pwd = getpwuid(getuid())))
    return (NULL);
  else
    username = my_str_copy(pwd->pw_name, 0);
  endpwent();
  return (username);
}

char	*get_hostname(char *nothing, t_shell *shell)
{
  int	fd;
  int	red;
  char	*ehost;
  char	host[50];

  (void)nothing;
  ehost = NULL;
  red = 0;
  if (!(ehost = clean_var(find_str_in_tab("HOST", shell->env)))
      && !(ehost = clean_var(find_str_in_tab("HOSTNAME", shell->env))))
    {
      if ((fd = open("/etc/hostname", O_RDONLY)) > 0
	  && (red = read(fd, host, 50 - 1)) >= 0)
	{
	  if (host[red - 1] == '\n')
	    red--;
	  host[red] = '\0';
	  return (my_str_copy(host, 0));
	}
    }
  return (ehost);
}

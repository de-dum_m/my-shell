/*
** my_memset.c for gnl in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat May 31 23:52:39 2014 armita_a
** Last update Sat May 31 23:52:42 2014 armita_a
*/

char	*my_memset(char *str, int len)
{
  int	i;

  i = 0;
  while (i < len)
    str[i++] = '\0';
  return (str);
}

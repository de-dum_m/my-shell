/*
** my_strstr.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Mon May 12 06:12:24 2014 chapui_s
** Last update Sat May 24 10:00:51 2014 armita_a
*/

#include "readline.h"
#include "tools.h"

char	*my_strstr(char *str, char *to_find)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      if (my_strncmp(str + i, to_find, my_strlen(to_find)) == 0)
	return (str + i);
      i += 1;
    }
  return ((void*)0);
}

/*
** my_getnbr.c for tools in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src/tools
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Apr 29 12:25:37 2014 armita_a
** Last update Sat May 24 09:56:45 2014 armita_a
*/

int	my_getnbr(char *str)
{
  int	index;
  int	nb;
  int	is_neg;

  nb = 0;
  index = 0;
  if (!str || !str[0])
    return (-1);
  while (str && (str[index] == '-' || str[index] == '+'))
    {
      nb++;
      index++;
    }
  if (nb % 2 == 0)
    is_neg = 1;
  else
    is_neg = -1;
  nb = 0;
  while (str && str[index] >= '0' && str[index] <= '9')
    nb = (nb * 10) + str[index++] - 48;
  if (str && str[index])
    return (-1);
  return (is_neg * nb);
}

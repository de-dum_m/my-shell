/*
** my_printf_tools.c for my_printf in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:32:18 2014 armita_a
** Last update Sun Jun  1 17:03:06 2014 de-dum_m
*/

#include <stdarg.h>
#include "my_printf.h"

int	my_put_hex(unsigned int ix, char type, int fd)
{
  char	*base;

  base = "0123456789abcdef";
  if (type == 'X')
    base = "0123456789ABCDEF";
  if ((my_count_putnbr_base(ix, base, fd)) == -1)
    return (-1);
  return (0);
}

int	my_putstr_weird(char *str, int fd)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 32 && str[i] < 127)
	{
	  if ((my_count_putchar(str[i], fd)) == -1)
	    return (-1);
	}
      else
	{
	  if ((my_count_putchar('\\', fd)) == -1)
	    return (-1);
	  if ((str[i] <= 7) && (my_count_putchar('0', fd)) == -1)
	    return (-1);
	  if ((str[i] <= 63) && (my_count_putchar('0', fd)) == -1)
	    return (-1);
	  if ((my_count_putnbr_base(str[i], "01234567", fd)) == -1)
	    return (-1);
	}
      i = i + 1;
    }
  return (0);
}

int	my_put_ptr(unsigned int ptr, int fd)
{
  if (ptr > 0)
    if ((my_count_putstr("0x", fd)) == -1)
      return (-1);
  if (ptr > 16777215)
    if ((my_count_putstr("7fff", fd)) == -1)
      return (-1);
  if (ptr == 0)
    {
      if ((my_count_putstr("(nil)", fd)) == -1)
	return (-1);
    }
  else if (ptr < 16777215 && ptr > 16777215)
    {
      if ((my_count_putstr("00", fd)) == -1)
	return (-1);
    }
  else if (ptr < 268435455 && ptr > 16777215)
    if ((my_count_putchar('0', fd)) == -1)
      return (-1);
  if (ptr > 0)
    if ((my_count_putnbr_base(ptr, "0123456789abcdef", fd)) == -1)
      return (-1);
  return (0);
}

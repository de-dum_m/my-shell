/*
** my_count_put_nbr_base.c for my_printf in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:31:58 2014 armita_a
** Last update Sun Jun  1 00:31:59 2014 armita_a
*/

#include "my_printf.h"

int	chop_chop_count(unsigned int nbr, int b, char *base, int fd)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  if (nbr / b)
    {
      if (((chop_chop_count(nbr / b, b, base, fd)) == -1)
	  || (chop_chop_count(nbr % b, b, base, fd)) == -1)
	return (-1);
    }
  if (nbr / b  == 0)
    {
      while ((unsigned int)i < nbr)
	{
	  i = i + 1;
	  j = j + 1;
	  if (i == b)
	    j = 0;
	}
      if ((my_count_putchar(base[j], fd)) == -1)
	return (-1);
    }
  return (0);
}

int	my_count_putnbr_base(unsigned int nbr, char *base, int fd)
{
  int	b;

  if (nbr > nbr + 1 || nbr + 100 < 100)
    return (nbr);
  b = 0;
  if (base[0] == '\0')
    return (0);
  while (base[b] != '\0')
    b = b + 1;
  if ((chop_chop_count(nbr, b, base, fd)) == -1)
    return (-1);
  return (nbr);
}

/*
** my_put_unsigned_nbr.c for my_printf in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:32:25 2014 armita_a
** Last update Sun Jun  1 00:32:26 2014 armita_a
*/

#include "my_printf.h"

int	print_modified_count(char nb, int fd)
{
  if ((my_count_putchar(nb + 48, fd)) == -1)
    return (-1);
  return (0);
}

int	my_put_unsigned_nbr(unsigned int nb, int fd)
{
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10, fd);
      my_count_put_nbr(nb % 10, fd);
    }
  else
    if ((print_modified_count(nb, fd)) == -1)
      return (-1);
  return (0);
}

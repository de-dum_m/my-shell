/*
** my_count_put_nbr.c for minitalk in /home/de-dum_m/rendu/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar 23 16:02:08 2014 de-dum_m
** Last update Fri May 30 11:38:07 2014 de-dum_m
*/

#include "my_printf.h"

int	my_count_put_nbr(int nb, int fd)
{
  if (nb < 0)
    {
      nb = - nb;
      if ((my_count_putchar('-', fd)) == -1)
	return (-1);
    }
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10, fd);
      my_count_put_nbr(nb % 10, fd);
    }
  else
    if ((print_modified_count(nb, fd)) == -1)
      return (-1);
  return (0);
}

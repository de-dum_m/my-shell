/*
** my_printf.c for my_printf in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:32:12 2014 armita_a
** Last update Sun Jun  1 00:32:13 2014 armita_a
*/

#include <stdarg.h>
#include <stdio.h>
#include "my_printf.h"

static int	det_type(va_list ap, char type, int fd)
{
  if (((type == 'd' || type == 'i') && my_count_put_nbr(va_arg(ap, int),
							fd) == -1)
      || ((type == 's') && my_count_putstr(va_arg(ap, char*), fd) == -1)
      || ((type == 'p') && my_put_ptr(va_arg(ap, unsigned int), fd) == -1)
      || ((type == 'u') && my_put_unsigned_nbr(va_arg(ap, unsigned int),
					       fd) == -1)
      || ((type == 'c' || type == 'C') && my_count_putchar(va_arg(ap, int),
							   fd) == -1)
      || ((type == 'x' || type == 'X') && my_put_hex(va_arg(ap, unsigned int),
						     type, fd) == -1)
      || ((type == 'b') && my_count_putnbr_base(va_arg(ap, unsigned int),
						"01", fd) == -1)
      || ((type == 'S') && my_putstr_weird(va_arg(ap, char*), fd) == -1)
      || ((type == 'o') && my_count_putnbr_base(va_arg(ap, unsigned int),
						"01234567", fd) == -1))
    return (-1);
  else if (type != 'd' && type != 's' && type != 'p' && type != 'u'
	   && type != 'c' && type != 'x' && type != 'b' && type != 'S'
	   && type != 'o' && type != 'i' && type != 'C' && type != 'X')
    return (0);
  return (1);
}

static int	my_dual_printf(const char *str, int fd, va_list ap)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '%' && str[i + 1] != '%')
	{
	  if (det_type(ap, str[i + 1], fd) == 0)
	    return (0);
	  i = i + 1;
	}
      else if (str[i] == '%')
	{
	  if ((my_count_putchar('%', fd)) == -1)
	    return (-1);
	  i = i + 1;
	}
      else
	if ((my_count_putchar(str[i], fd)) == -1)
	  return (-1);
      i = i + 1;
    }
  va_end(ap);
  return (count_prints() - 1);
}

int		my_print_error(const char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  if ((my_dual_printf(str, 2, ap)) == -1)
    return (-1);
  return (0);
}

int		my_printf(const char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  return (my_dual_printf(str, 1, ap));
}

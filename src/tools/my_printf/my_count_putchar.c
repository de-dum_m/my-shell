/*
** my_count_putchar.c for my_printf in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh/src
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sun Jun  1 00:32:06 2014 armita_a
** Last update Sun Jun  1 16:43:07 2014 de-dum_m
*/

#include <unistd.h>

int		count_prints()
{
  static int	prints;

  prints = prints + 1;
  return (prints);
}

int	my_count_putchar(char c, int fd)
{
  if (!c)
    return (0);
  else
    {
      if ((write(fd, &c, 1)) == -1)
	return (-1);
    }
  count_prints();
  return (0);
}

int	my_count_putstr(char *str, int fd)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      if ((my_count_putchar(str[i], fd)) == -1)
	return (-1);
      i = i + 1;
    }
  return (0);
}

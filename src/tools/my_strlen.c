/*
** my_strlen.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Tue May  6 23:51:55 2014 chapui_s
** Last update Sat May 31 17:25:30 2014 armita_a
*/

int	my_strlen(char *s)
{
  int	size;

  size = 0;
  while (s && s[size])
    size += 1;
  return (size);
}

/*
** free.c for mysh in /home/bakhou_r//projet/parser
**
** Made by bakhou_r
** Login   <bakhou_r@epitech.net>
**
** Started on  Thu May  8 10:08:29 2014 raphael bakhouche
** Last update Sun Jun  1 17:09:54 2014 de-dum_m
*/

#include <stdlib.h>

void	free_tab(char **tab)
{
  int	i;

  i = 0;
  while (tab && tab[i])
    free(tab[i++]);
  free(tab);
}

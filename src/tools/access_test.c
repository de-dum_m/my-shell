/*
** access_test.c for tools in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue May 13 18:41:41 2014 de-dum_m
** Last update Sun Jun  1 12:38:25 2014 de-dum_m
*/

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "tools.h"

int	access_test_exe(char *file)
{
  struct stat	buf;

  if (stat(file, &buf) == -1)
    return (FAILURE);
  if ((buf.st_mode & S_IFMT) == S_IFDIR)
    return (FAILURE);
  if (access(file, X_OK) == 0)
    return (SUCCESS);
  else if (access(file, F_OK) == 0)
    my_print_error("%s: permission denied\n", file);
  return (FAILURE);
}

/*
** my_strcat.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Fri May  9 20:47:43 2014 chapui_s
** Last update Sat May 31 08:03:05 2014 chapui_s
*/

#include <stdlib.h>
#include "tools.h"
#include "readline.h"

char	*str_cat(char *s1, char *s2)
{
  char	*dest;
  int	i;
  int	j;

  i = 0;
  j = 0;
  if ((dest = (char*)malloc(sizeof(char) * (my_strlen(s1) + my_strlen(s2)
					    + 2))) == NULL)
    return ((void*)(unsigned long)my_putstr("error: could not alloc\n", 1));
  while (s1 && s1[i])
    {
      dest[i] = s1[i];
      i += 1;
    }
  while (s2 && s2[j])
    {
      dest[i + j] = s2[j];
      j += 1;
    }
  dest[i + j] = 0;
  dest[i + j + 1] = 0;
  return (dest);
}

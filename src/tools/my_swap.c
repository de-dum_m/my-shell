/*
** my_swap.c for tools in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Wed May  7 15:59:59 2014 armita_a
** Last update Wed May  7 16:29:38 2014 armita_a
*/

void	my_swap_str(char **s1, char **s2)
{
  char	*temp;

  temp = *s2;
  *s2 = *s1;
  *s1 = temp;
}

/*
** is_in_str.c for tools in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue May  6 11:17:59 2014 armita_a
** Last update Tue May  6 11:19:26 2014 armita_a
*/

#include "tools.h"

int	is_in_str(char *str, char c)
{
  int	index;

  index = 0;
  while (str && str[index])
    if (str[index++] == c)
      return (SUCCESS);
  return (FAILURE);
}

/*
** append_file_to_fd.c for tools in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue May 13 18:01:16 2014 de-dum_m
** Last update Sat May 24 09:55:48 2014 armita_a
*/

#include <fcntl.h>
#include <unistd.h>
#include "tools.h"

int	append_file_to_fd(char *file, int fd)
{
  int	src_fd;
  int	red;
  char	buffer[1024];

  if ((src_fd = open(file, O_RDONLY)) == -1)
    return (FAILURE);
  while ((red = read(src_fd, buffer, 1024 - 1)) > 0)
    {
      buffer[red] = '\0';
      if (write(fd, buffer, red) != red)
	return (FAILURE);
    }
  close(src_fd);
  return (SUCCESS);
}

/*
** tab_copy.c for tools in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue May 20 16:33:21 2014 de-dum_m
** Last update Tue May 20 16:57:24 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"

char	**my_tab_copy(char **tab)
{
  int	i;
  int	tab_len;
  char	**tab_copy;

  i = 0;
  tab_len = my_tablen(tab);
  if (!(tab_copy = malloc(sizeof(char *) * (tab_len + 1))))
    return (NULL);
  while (i < tab_len && tab[i])
    {
      tab_copy[i] = my_strdup(tab[i]);
      i++;
    }
  tab_copy[i] = NULL;
  return (tab_copy);
}

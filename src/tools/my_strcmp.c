/*
** my_strcmp.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Mon Apr 14 16:34:00 2014
** Last update Fri May 16 17:27:18 2014 de-dum_m
*/

#include <stdlib.h>

int	my_strcmp(char *s1, char *s2)
{
  if (s1 == NULL && s2 != NULL)
    return (1);
  return ((*s1 == *s2 && *s1) ? (my_strcmp(s1 + 1, s2 + 1)) : (*s1 - *s2));
}

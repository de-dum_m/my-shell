/*
** my_tablen.c for tools in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue May  6 10:11:56 2014 armita_a
** Last update Thu May  8 12:47:29 2014 armita_a
*/

int	my_tablen(char **tab)
{
  int	index;

  index = 0;
  while (tab && tab[index])
    index++;
  return (index);
}

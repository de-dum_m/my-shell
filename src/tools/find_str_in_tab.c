/*
** find_str_in_tab.c for tools in /home/armita_a/Documents/Teck_1/System_unix/psu_2013_42sh
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Wed Apr 30 13:30:52 2014 armita_a
** Last update Sat May 31 17:27:20 2014 armita_a
*/

#include <stdlib.h>

char	*find_str_in_tab(char *str, char **env)
{
  int	index;
  int	count;

  index = 0;
  while (env && env[index] && str)
    {
      count = 0;
      while (str[count] && env[index][count]
	     && env[index][count] == str[count])
        count++;
      if (!str[count] && env[index][count] == '=')
	return (env[index]);
      index++;
    }
  return (NULL);
}

/*
** my_strdup.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Mon May 12 06:20:20 2014 chapui_s
** Last update Sat May 24 21:23:22 2014 chapui_s
*/

#include <stdlib.h>
#include "readline.h"
#include "tools.h"

char	*my_strdup(const char *s)
{
  char	*new;
  int	i;

  i = 0;
  if (!s)
    return (NULL);
  if ((new = (char*)malloc(my_strlen((char*)s) + 3)) == NULL)
    return ((void*)(unsigned long)my_putstr("error: could not alloc\n", 1));
  while (s[i])
    {
      new[i] = s[i];
      i += 1;
    }
  new[i] = 0;
  return (new);
}

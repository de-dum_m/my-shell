/*
** my_str_to_wordtab.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Fri May  9 20:00:07 2014 chapui_s
** Last update Sat May 24 09:58:01 2014 armita_a
*/

#include <stdlib.h>
#include "readline.h"

static int	is_it_alpha(char c)
{
  if (c != ' ' && c != '\n')
    return (1);
  return (0);
}

static int	count_words(char *str)
{
  int		word;
  int		i;
  int		nb_words;

  nb_words = 0;
  i = 0;
  word = 0;
  while (str[i])
    {
      if (word == 0 && is_it_alpha(str[i]) == 1)
	{
	  word = 1;
	  nb_words = nb_words + 1;
	}
      if (word == 1 && is_it_alpha(str[i]) == 0)
	word = 0;
      i = i + 1;
    }
  return (nb_words);
}

static void	dup_n_car(char *new_str, char *str, int begin, int end)
{
  int		i;

  i = 0;
  while (begin <= end)
    {
      new_str[i] = str[begin];
      begin = begin + 1;
      i = i + 1;
    }
  new_str[i] = '\0';
}

static int	save_words(char **tab_c, char *str, int nb_words)
{
  int		i;
  int		begin;
  int		word_cur;

  word_cur = 0;
  i = 0;
  begin = 0;
  while (word_cur < nb_words)
    {
      while (str[i] && is_it_alpha(str[i]) == 0)
	i = i + 1;
      begin = i;
      while (str[i] && is_it_alpha(str[i]) == 1)
	i = i + 1;
      if ((tab_c[word_cur] = (char *)malloc(sizeof(char)
					    * ((i - 1) - begin + 3))) == NULL)
	return (my_putstr("error: could not alloc\n", 2));
      dup_n_car(tab_c[word_cur], str, begin, i - 1);
      i = i + 1;
      word_cur = word_cur + 1;
    }
  return (0);
}

char	**my_str_to_wordtab(char *str)
{
  int	nb_words;
  char	**tab_c;

  nb_words = count_words(str);
  if ((tab_c = (char**)malloc(sizeof(char*) * (nb_words + 1))) == NULL)
    return ((void*)(unsigned long)my_putstr("error: could not alloc\n", 1));
  if ((save_words(tab_c, str, nb_words)) == -1)
    return (NULL);
  tab_c[nb_words] = NULL;
  return (tab_c);
}

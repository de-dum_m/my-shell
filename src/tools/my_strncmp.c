/*
** my_strncmp.c for 42sh in /home/chapui_s/travaux/42sh
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Tue May  6 23:50:21 2014 chapui_s
** Last update Sat May 24 10:00:41 2014 armita_a
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      else if (s1[i] == '\0')
	return (0);
      i = i + 1;
    }
  return (0);
}

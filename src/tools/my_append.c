/*
** my_append.c for 42sh in /home/de-dum_m/code/B2-systeme_unix/psu_2013_42sh
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Tue May 13 18:57:48 2014 de-dum_m
** Last update Wed May 28 09:14:25 2014 de-dum_m
*/

#include <stdlib.h>
#include "tools.h"

int	append_one_char(t_ps *pr, char c, int *j)
{
  int	i;
  char	*res;

  i = 0;
  if (!pr)
    return (FAILURE);
  if (j)
    (*j)++;
  if (c == '\n')
    pr->len = 0;
  else
    pr->len++;
  if (!(res = malloc(my_strlen(pr->str) + 3)))
    return (FAILURE);
  while (pr->str && pr->str[i])
    {
      res[i] = pr->str[i];
      i++;
    }
  res[i++] = c;
  res[i] = '\0';
  free(pr->str);
  pr->str = res;
  return (SUCCESS);
}

char	*my_append(char *s1, char *s2)
{
  int	i;
  int	j;
  char	*res;

  i = 0;
  j = 0;
  if (!(res = malloc(my_strlen(s1) + my_strlen(s2) + 3)))
    return (NULL);
  while (s1 && s1[i])
    {
      res[i] = s1[i];
      i++;
    }
  while (s2 && s2[j])
    res[i++] = s2[j++];
  res[i] = '\0';
  return (res);
}

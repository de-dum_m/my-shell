/*
** list_input.c for 42sh in /home/chapui_s/travaux/42sh/parser
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 22:04:04 2014 chapui_s
** Last update Sun Jun  1 00:10:59 2014 armita_a
*/

#include <stdlib.h>
#include "parser.h"
#include "readline.h"

static t_input	*create_input(char *file, char *heredoc, int is_pipe)
{
  t_input	*new;

  if ((new = (t_input*)malloc(sizeof(*new))))
    {
      new->file = file;
      new->heredoc = heredoc;
      new->is_pipe = is_pipe;
      new->next = NULL;
    }
  else
    if ((my_putstr("error: could not alloc\n", 2)) == -1)
      return (NULL);
  return (new);
}

int		push_input(t_input **input, char *file, char *heredoc, int pipe)
{
  t_input	*tmp;

  tmp = *input;
  if (tmp)
    {
      while (tmp->next)
	tmp = tmp->next;
      if ((tmp->next = create_input(file, heredoc, pipe)) == NULL)
	return (-1);
    }
  else
    if ((*input = create_input(file, heredoc, pipe)) == NULL)
      return (-1);
  return (0);
}

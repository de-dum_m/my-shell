/*
** about_var.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/parser
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sat May 17 15:34:29 2014 chapui_s
** Last update Sat May 31 23:53:13 2014 armita_a
*/

static void	count_var(char *str, int *i)
{
  int		j;

  j = *i + 1;
  while (str[j] && str[j + 1] && str[j] != ')')
    {
      if (str[j] == '$' && str[j + 1] == '(')
	count_var(str, &j);
      else
	j += 1;
    }
  *i = j + 1;
}

static void	count_quote(char *str, int *i)
{
  int		j;

  j = *i + 1;
  while (str[j] && str[j] != '`')
    j += 1;
  *i = j + 1;
}

static void	count_little_quote(char *str, int *i)
{
  int		j;

  j = *i + 1;
  while (str[j] && str[j] != '\'')
    j += 1;
  *i = j + 1;
}

static void	count_normal_quote(char *str, int *i)
{
  int		j;

  j = *i + 1;
  while (str[j] && str[j] != '"')
    j += 1;
  *i = j + 1;
}

void	pass_var(char *str, int *i)
{
  if (str[*i] == '$' && str[*i + 1] == '(')
    count_var(str, i);
  else if (str[*i] == '`')
    count_quote(str, i);
  else if (str[*i] == '\'')
    count_little_quote(str, i);
  else if (str[*i] == '"')
    count_normal_quote(str, i);
}

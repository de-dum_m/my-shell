/*
** clean_redirections.c for 42sh in /home/chapui_s/travaux/42sh/parser
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Thu May 15 00:39:26 2014 chapui_s
** Last update Sat May 31 06:23:10 2014 chapui_s
*/

#include <stdlib.h>
#include "parser.h"
#include "tools.h"

void	clean_this_redirection(char *str)
{
  int	i;

  i = 0;
  while (str[i] && (str[i] == '>' || str[i] == '<'))
    {
      str[i] = ' ';
      i += 1;
    }
  while (str[i] && str[i] == ' ')
    i += 1;
  while (str[i] && str[i] != ' ' && str[i] != '|' && str[i] != '&'
	 && str[i] != ';')
    {
      str[i] = ' ';
      i += 1;
    }
}

void	clean_redirections(char *str)
{
  int	i;
  int	size;

  i = 0;
  size = my_strlen(str);
  while (str[i])
    {
      pass_var(str, &i);
      if (str[i] == '>' || str[i] == '<')
	clean_this_redirection(str + i);
      if (i >= size)
	return ;
      if (str[i])
	i += 1;
    }
}

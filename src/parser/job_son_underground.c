/*
** job_son_underground.c for 42sh in /home/chapui_s/rendu/psu_2013_42sh/src/readline
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Sun May 25 04:13:45 2014 chapui_s
** Last update Sun Jun  1 16:38:12 2014 de-dum_m
*/

#include <unistd.h>
#include <stdlib.h>
#include "parser.h"
#include "tools.h"
#include "builtins.h"
#include "shell.h"
#include "readline.h"
#include "shell.h"

static int	loop_underground(t_proc *proc, t_shell *shell)
{
  int		nb;

  nb = SUCCESS;
  while (proc && nb == SUCCESS)
    {
      if ((nb = my_execute(proc, shell)) == FAILURE)
	my_print_error("42sh : command not found: %s\n", proc->cmd);
      proc = go_next(proc, nb);
    }
  return (0);
}

int	do_the_job_son(int pipefd[2], t_shell *shell, t_proc *proc)
{
  if ((close(pipefd[0])) == -1)
    return (my_putstr("error: close pipe\n", 2));
  if ((dup2(pipefd[1], 1)) == -1)
    return (my_putstr("error: dup2\n", 2));
  loop_underground(proc, shell);
  if ((write(1, "\0", 1)) == -1)
    return (-1);
  if ((close(pipefd[1])) == -1)
    return (my_putstr("error: close pipe\n", 2));
  return (-1);
}

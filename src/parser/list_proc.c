/*
** list_proc.c for 42sh in /home/chapui_s/travaux/42sh/parser
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 21:44:26 2014 chapui_s
** Last update Sun May 25 04:43:55 2014 chapui_s
*/

#include <stdlib.h>
#include "readline.h"
#include "parser.h"
#include "tools.h"

static int	select_mode(char *mode)
{
  if (my_strncmp(mode, "&&", 2) == 0)
    return (IF_OK);
  else if (my_strncmp(mode, "&", 1) == 0)
    return (BACKGROUND);
  else if (my_strncmp(mode, "||", 2) == 0)
    return (IF_ERR);
  return (FAILURE);
}

static t_proc	*create_proc(char *cmd, char *mode, t_proc *prev)
{
  t_proc	*new;

  if ((new = (t_proc*)malloc(sizeof(*new))))
    {
      new->cmd = cmd;
      new->cmd_argv = NULL;
      new->path_cmd = NULL;
      new->pid = FAILURE;
      new->pgid = FAILURE;
      new->outfd = FAILURE;
      new->infd = FAILURE;
      new->mode = select_mode(mode);
      new->status = FAILURE;
      new->disowned = FAILURE;
      new->output = NULL;
      new->input = NULL;
      new->next = NULL;
      new->prev = prev;
    }
  else
    if ((my_putstr("error: could not alloc\n", 2)) == -1)
      return (NULL);
  return (new);
}

int		push_proc(t_proc **proc, char *cmd, char *mode,
			  t_proc **tmp_proc)
{
  t_proc	*tmp;

  tmp = *proc;
  if (tmp)
    {
      while (tmp->next)
	tmp = tmp->next;
      if ((tmp->next = create_proc(cmd, mode, tmp)) == NULL)
	return (-1);
      tmp = tmp->next;
    }
  else
    {
      if ((*proc = create_proc(cmd, mode, NULL)) == NULL)
	return (-1);
      tmp = *proc;
    }
  if ((my_strcmp(mode, "|")) == 0)
    if (push_output(&(tmp->output), NULL, 0, SUCCESS) == -1)
      return (-1);
  if (tmp->prev && tmp->prev->output && tmp->prev->output->is_pipe == SUCCESS)
    if (push_input(&(tmp->input), NULL, NULL, SUCCESS) == -1)
      return (-1);
  *tmp_proc = tmp;
  return (0);
}

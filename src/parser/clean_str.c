/*
** clean_str.c for 42sh in /home/chapui_s/travaux/42sh/parser
**
** Made by chapui_s
** Login   <chapui_s@epitech.eu>
**
** Started on  Wed May 14 18:10:36 2014 chapui_s
** Last update Fri May 30 09:34:40 2014 armita_a
*/

static void	last_clean(char *str, int i)
{
  i -= 1;
  while (i > 0 && str[i] == ' ')
    {
      str[i] = 0;
      i -= 1;
    }
}

char	*clean_str(char *str)
{
  int	i;
  int	j;

  i = 0;
  while (str[i])
    {
      j = i;
      if (str[i] == '\t')
	str[i] = ' ';
      if ((str[i] == ' ' && (str[i + 1] == ' ' || str[i + 1] == '\t'))
	  || (i == 0 && str[i] == ' '))
	{
	  while (str[j])
	    {
	      str[j] = str[j + 1];
	      j += 1;
	    }
	  i -= 1;
	}
      i += 1;
    }
  last_clean(str, i);
  return (str);
}

int	is_bad_character(char *str)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      if (str[i] < 32 || str[i] > 126)
	if (str[i] != '\n' && str[i] != '\t')
	  return (str[i]);
      i += 1;
    }
  return (0);
}
